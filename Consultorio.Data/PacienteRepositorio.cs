﻿using Aplicacion.Data;
using Consultorio.Entidades;
using Consultorio.Repositorio;
using System;
using System.Data;

namespace Consultorio.Data
{
  public class PacienteRepositorio : Repositorio<Paciente>, IPacienteRepositorio
  {
    public override int Agregar(Paciente unaEntidad)
    {
      string sql = string.Format(
        "INSERT INTO {0} " +
        "(id, nombre_apellido, dni, email, telefono, fecha_nacimiento, sexo_id) " +
        "VALUES " +
        "(@id, @nombre_apellido, @dni, @email, @telefono, @fecha_nacimiento, @sexo_id)",
        _table);

      unaEntidad.Id = Guid.NewGuid();

      IDataParameter[] parametros =
      {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@nombre_apellido", unaEntidad.NombreApellido),
        _acceso.CreateParameter("@dni", unaEntidad.Dni),
        _acceso.CreateParameter("@email", unaEntidad.Email),
        _acceso.CreateParameter("@telefono", unaEntidad.Telefono),
        _acceso.CreateParameter("@fecha_nacimiento", unaEntidad.FechaNacimiento),
        _acceso.CreateParameter("@sexo_id", (int)unaEntidad.Sexo)
      };

      return _acceso.Update(sql, parametros);
    }
  }
}
