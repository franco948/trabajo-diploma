﻿using System;
using System.ComponentModel;

namespace Aplicacion.Entidades
{
  public abstract class Entidad
  {
    [Browsable(false)]
    public Guid Id { get; set; }
  }
}
