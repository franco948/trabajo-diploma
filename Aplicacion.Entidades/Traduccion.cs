﻿namespace Aplicacion.Entidades
{
  // hereda una clase que no utiliza para poder utilizar repositorio base
  public class Traduccion : Entidad
  {
    public string Texto { get; set; }

    public Idioma Idioma { get; set; }
    public Tag Tag { get; set; }
  }
}
