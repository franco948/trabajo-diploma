﻿using System;

namespace Aplicacion.Entidades
{
  public class Bitacora : Entidad
  {
    public Bitacora()
    {
      this.FechaHora = DateTime.Now;
    }

    public Eventos Evento { get; set; }
    //public string Descripcion { get; set; }
    public DateTime FechaHora { get; set; }

    public Usuario Usuario { get; set; }
  }
}
