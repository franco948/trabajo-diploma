﻿using System;
using System.Collections.Generic;

namespace Aplicacion.Entidades
{
  public class Usuario : Verificable, ICloneable
  {
    public string NombreUsuario { get; set; }
    public string Contraseña { get; set; }

    public virtual List<Patente> Permisos { get; set; }
    public Idioma Idioma { get; set; }

    public override string ToString()
    {
      return this.NombreUsuario;
    }

    public object Clone()
    {
      return new Usuario
      {
        Id = this.Id,
        NombreUsuario = this.NombreUsuario,
        Contraseña = this.Contraseña,
        Dvh = this.Dvh,
        Idioma = new Idioma { Id = this.Idioma.Id }
      };
    }
  }
}

