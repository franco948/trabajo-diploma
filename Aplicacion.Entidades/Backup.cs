﻿using System;

namespace Aplicacion.Entidades
{
  public class Backup : Entidad
  {
    public string Directorio { get; set; }
    public string Nombre { get; set; }
    public DateTime FechaHora { get; set; }
  }
}
