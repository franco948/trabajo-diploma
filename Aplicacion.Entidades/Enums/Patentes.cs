﻿namespace Aplicacion.Entidades
{
  public class Patentes
  {
    protected Patentes(int internalValue)
    {
      this.InternalValue = InternalValue;
    }

    public static readonly Patentes Backups = new Patentes(1);
    public static readonly Patentes CambiarIdioma = new Patentes(2);
    public static readonly Patentes Familias = new Patentes(3);
    public static readonly Patentes Idiomas = new Patentes(4);
    public static readonly Patentes Integridad = new Patentes(5);
    public static readonly Patentes Permisos = new Patentes(6);
    public static readonly Patentes Usuarios = new Patentes(7);

    public int InternalValue { get; protected set; }
  }
}
