﻿using System;

namespace Aplicacion.Entidades
{
  public class BitacoraUsuario : Entidad
  {
    public Usuario Usuario { get; set; }
    public DateTime FechaModificacion { get; set; }
  }
}
