﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Aplicacion.Entidades
{
  public abstract class Verificable : Entidad
  {
    public string Dvh { get; set; }

    public string CalcularDvh()
    {
      object[] argumentos = { this.ObtenerCadena() };

      Type tipo = Assembly.Load("Aplicacion.Seguridad")
         .GetType("Aplicacion.Seguridad.CryptoManager");

      MethodInfo method = tipo.GetMethod("Cifrar", new Type[] { typeof(string) });

      return (string)method.Invoke(null, argumentos);
    }

    private string ObtenerCadena()
    {
      string dvh = string.Empty;

      Type tipo = this.GetType();

      PropertyInfo[] atributos = tipo.GetProperties();

      foreach (PropertyInfo atributo in atributos)
      {
        if (atributo.PropertyType.IsClass && atributo.PropertyType.Name != "String")
        {
          if (atributo.PropertyType.IsSubclassOf(typeof(Entidad)))
          {
            var entidad = atributo.GetValue(this);

            dvh += entidad.GetType().GetProperty("Id").GetValue(entidad);
          }
        }
        else if (!(atributo.Name == "Dvh" || atributo.Name == "Id" ||
            atributo.PropertyType.IsAssignableFrom(typeof(IEnumerable<object>))))
          dvh += atributo.GetValue(this);
      }

      return dvh;
    }
  }
}
