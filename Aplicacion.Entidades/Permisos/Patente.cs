﻿using System;
using System.Collections.Generic;

namespace Aplicacion.Entidades
{
  public class Patente : Entidad
  {
    public string Nombre { get; set; }
    public Patentes Permiso { get; set; }

    public virtual void Agregar(Patente unPermiso)
    {
      throw new NotImplementedException();
    }

    public virtual List<Patente> Listar()
    {
      return new List<Patente>() { this };
    }

    //public virtual bool Existe(Patente unPermiso)
    //{
    //  if (unPermiso is Familia)
    //    foreach (Patente permiso in unPermiso.Listar())
    //    {
    //      if (permiso.Id == this.Id)
    //        return true;
    //    }

    //  return this.Id == unPermiso.Id;
    //}

    public virtual bool Existe(Patentes unPermiso)
    {
      return this.Permiso.InternalValue == unPermiso.InternalValue;
    }

    public override string ToString()
    {
      return this.Nombre.ToString();
    }

    public virtual List<Patente> DirectChilds
    {
      get { return null; }
    }

    //public override bool Equals(object obj)
    //{
    //  return this.Nombre == ((Permisos)obj).ToString();
    //}
  }
}
