﻿using System.Collections.Generic;
using System.Linq;

namespace Aplicacion.Entidades
{
  public class Familia : Patente
  {
    private readonly List<Patente> _permisos;

    public Familia()
    {
      _permisos = new List<Patente>();
    }

    public override void Agregar(Patente unPermiso)
    {
      _permisos.Add(unPermiso);
    }

    public override List<Patente> Listar()
    {
      var permisos = new List<Patente>();

      foreach (var permiso in _permisos)
      {
        permisos.AddRange(permiso.Listar());
      }

      return permisos;
    }

    public override bool Existe(Patentes unPermiso)
    {
      foreach (Patente permiso in _permisos)
      {
        if (permiso.Listar().
          Any(x => x.Permiso.InternalValue == unPermiso.InternalValue))
          return true;
      }

      return false;
    }

    public override List<Patente> DirectChilds
    {
      get
      {
        return _permisos;
      }
    }
  }
}
