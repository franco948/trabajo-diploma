﻿namespace Aplicacion.Entidades
{
  public class Idioma : Entidad
  {
    public string Nombre { get; set; }

    public override string ToString()
    {
      return this.Nombre;
    }
  }
}
