﻿using Consultorio.Servicio;
using Consultorio.Servicio.Interfaces;
using Ninject.Modules;

namespace Consultorio.DependencyResolution
{
  public class ConsultorioServicioModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IPacienteServicio>().To<PacienteServicio>();
    }
  }
}
