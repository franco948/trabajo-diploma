﻿using Consultorio.Data;
using Consultorio.Repositorio;
using Ninject.Modules;

namespace Consultorio.DependencyResolution
{
  public class ConsultorioRepositorioModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IPacienteRepositorio>().To<PacienteRepositorio>();
    }
  }
}
