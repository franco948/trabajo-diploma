﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Aplicacion.Tests
{
  [TestClass]
  public class UnitTest1
  {
    [TestMethod]
    public void Calcular_Dvh()
    {
      // arrange
      Usuario usuario = new Usuario
      {
        Id = Guid.NewGuid(),
        NombreUsuario = "franco94",
        Contraseña = "123456",
        Idioma = new Idioma { Id = Guid.NewGuid() }
      };

      string cadenaACifrar = usuario.NombreUsuario + usuario.Contraseña +
        usuario.Idioma.Id;

      string dvhEsperado = CryptoManager.Cifrar(cadenaACifrar);

      // act
      string dvh = usuario.CalcularDvh();

      // assert
      Assert.AreEqual(dvh, dvhEsperado);
    }
  }
}
