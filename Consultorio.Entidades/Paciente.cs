﻿using Aplicacion.Entidades;
using System;

namespace Consultorio.Entidades
{
  public class Paciente : Entidad
  {
    public string NombreApellido { get; set; }
    public int Dni { get; set; }
    public string Email { get; set; }
    public int Telefono { get; set; }
    public DateTime FechaNacimiento { get; set; }
    public Generos Sexo { get; set; }
  }
}
