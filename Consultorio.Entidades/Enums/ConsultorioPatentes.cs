﻿using Aplicacion.Entidades;

namespace Consultorio.Entidades
{
  public class ConsultorioPatentes : Patentes
  {
    public ConsultorioPatentes(int internalValue) : base(internalValue)
    {
    }

    public static readonly ConsultorioPatentes AltaPaciente =
      new ConsultorioPatentes(8);
  }
}
