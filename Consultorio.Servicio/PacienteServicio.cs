﻿using Aplicacion.Servicio;
using Consultorio.Entidades;
using Consultorio.Repositorio;
using Consultorio.Servicio.Interfaces;

namespace Consultorio.Servicio
{
  public class PacienteServicio : Servicio<Paciente>, IPacienteServicio
  {
    public PacienteServicio(IPacienteRepositorio unRepositorio) :
      base(unRepositorio)
    {
    }
  }
}
