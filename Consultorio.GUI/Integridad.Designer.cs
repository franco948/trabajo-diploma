﻿namespace Consultorio.GUI
{
  partial class Integridad
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnVerificar = new System.Windows.Forms.Button();
      this.cbxTablas = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.dgvFilasAfectadas = new System.Windows.Forms.DataGridView();
      this.label2 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.dgvFilasAfectadas)).BeginInit();
      this.SuspendLayout();
      // 
      // btnVerificar
      // 
      this.btnVerificar.Location = new System.Drawing.Point(15, 52);
      this.btnVerificar.Name = "btnVerificar";
      this.btnVerificar.Size = new System.Drawing.Size(121, 23);
      this.btnVerificar.TabIndex = 0;
      this.btnVerificar.Text = "Verificar";
      this.btnVerificar.UseVisualStyleBackColor = true;
      this.btnVerificar.Click += new System.EventHandler(this.btnVerificar_Click);
      // 
      // cbxTablas
      // 
      this.cbxTablas.FormattingEnabled = true;
      this.cbxTablas.Location = new System.Drawing.Point(15, 25);
      this.cbxTablas.Name = "cbxTablas";
      this.cbxTablas.Size = new System.Drawing.Size(121, 21);
      this.cbxTablas.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(34, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Tabla";
      // 
      // dgvFilasAfectadas
      // 
      this.dgvFilasAfectadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvFilasAfectadas.Location = new System.Drawing.Point(15, 109);
      this.dgvFilasAfectadas.Name = "dgvFilasAfectadas";
      this.dgvFilasAfectadas.Size = new System.Drawing.Size(655, 168);
      this.dgvFilasAfectadas.TabIndex = 3;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 93);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(78, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Filas afectadas";
      // 
      // Integridad
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(687, 292);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.dgvFilasAfectadas);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxTablas);
      this.Controls.Add(this.btnVerificar);
      this.Name = "Integridad";
      this.Text = "Integridad";
      ((System.ComponentModel.ISupportInitialize)(this.dgvFilasAfectadas)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnVerificar;
    private System.Windows.Forms.ComboBox cbxTablas;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.DataGridView dgvFilasAfectadas;
    private System.Windows.Forms.Label label2;
  }
}