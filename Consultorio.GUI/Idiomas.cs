﻿using Aplicacion.Entidades;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Idiomas : Form
  {
    // agregar AgregarIdioma a TraduccionServicio
    private IKernel _kernel;
    private ITraduccionServicio _traduccionServicio;
    private ITagServicio _tagServicio;
    private IIdiomaServicio _idiomaServicio;

    private Idioma _idioma;
    private IEnumerable<Tag> _tags;
    private List<Traduccion> _traducciones;
    private Traduccion _traduccion;
    private int _indice;

    public Idiomas(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;
      _traduccionServicio = _kernel.Get<ITraduccionServicio>();
      _tagServicio = _kernel.Get<ITagServicio>();
      _idiomaServicio = _kernel.Get<IIdiomaServicio>();

      _indice = 0;
      _idioma = new Idioma();
      _traducciones = new List<Traduccion>();
      _tags = _tagServicio.ObtenerTodos();

      foreach (Tag tag in _tags)
      {
        Traduccion traduccion = new Traduccion
        {
          //Idioma = _idioma,
          Tag = tag
        };

        _traducciones.Add(traduccion);
      }

      _traduccion = _traducciones[0];

      this.Cargar();

      Controlador.CargarGrilla(dgvTraducciones, _tags);
    }

    private void btnAgregarTraduccion_Click(object sender, System.EventArgs e)
    {
      _traduccion.Texto = txtTraduccion.Text;
    }

    private void btnAnterior_Click(object sender, System.EventArgs e)
    {
      if (_indice != 0)
      {
        _indice--;
        _traduccion = _traducciones[_indice];

        this.Cargar();
      }
    }

    private void btnSiguiente_Click(object sender, System.EventArgs e)
    {
      if (_indice < _traducciones.Count - 1)
      {
        _indice++;
        _traduccion = _traducciones[_indice];

        this.Cargar();
      }
    }

    private void Cargar()
    {
      txtTag.Text = _traduccion.Tag.Nombre;
      txtTraduccion.Text = _traduccion.Texto;
    }

    private void btnAgregarIdioma_Click(object sender, System.EventArgs e)
    {
      _idioma = new Idioma
      {
        Nombre = txtIdioma.Text
      };

      _idiomaServicio.Agregar(_idioma);

      foreach (Traduccion traduccion in _traducciones)
      {
        traduccion.Idioma = _idioma;
        _traduccionServicio.Agregar(traduccion);
      }
    }
  }
}
