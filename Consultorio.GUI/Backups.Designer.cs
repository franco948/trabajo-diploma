﻿namespace Consultorio.GUI
{
  partial class Backups
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnBackup = new System.Windows.Forms.Button();
      this.btnRestore = new System.Windows.Forms.Button();
      this.txtDirectorio = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.dgvBackups = new System.Windows.Forms.DataGridView();
      this.lblNombre = new System.Windows.Forms.Label();
      this.txtNombreBackup = new System.Windows.Forms.TextBox();
      ((System.ComponentModel.ISupportInitialize)(this.dgvBackups)).BeginInit();
      this.SuspendLayout();
      // 
      // btnBackup
      // 
      this.btnBackup.Location = new System.Drawing.Point(364, 35);
      this.btnBackup.Name = "btnBackup";
      this.btnBackup.Size = new System.Drawing.Size(132, 23);
      this.btnBackup.TabIndex = 0;
      this.btnBackup.Text = "Backup";
      this.btnBackup.UseVisualStyleBackColor = true;
      this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
      // 
      // btnRestore
      // 
      this.btnRestore.Location = new System.Drawing.Point(364, 64);
      this.btnRestore.Name = "btnRestore";
      this.btnRestore.Size = new System.Drawing.Size(132, 23);
      this.btnRestore.TabIndex = 1;
      this.btnRestore.Text = "Restore";
      this.btnRestore.UseVisualStyleBackColor = true;
      this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
      // 
      // txtDirectorio
      // 
      this.txtDirectorio.Location = new System.Drawing.Point(12, 25);
      this.txtDirectorio.Name = "txtDirectorio";
      this.txtDirectorio.Size = new System.Drawing.Size(132, 20);
      this.txtDirectorio.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(52, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Directorio";
      // 
      // dgvBackups
      // 
      this.dgvBackups.AllowUserToAddRows = false;
      this.dgvBackups.AllowUserToDeleteRows = false;
      this.dgvBackups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvBackups.Location = new System.Drawing.Point(12, 93);
      this.dgvBackups.Name = "dgvBackups";
      this.dgvBackups.ReadOnly = true;
      this.dgvBackups.Size = new System.Drawing.Size(484, 230);
      this.dgvBackups.TabIndex = 4;
      // 
      // lblNombre
      // 
      this.lblNombre.AutoSize = true;
      this.lblNombre.Location = new System.Drawing.Point(12, 51);
      this.lblNombre.Name = "lblNombre";
      this.lblNombre.Size = new System.Drawing.Size(44, 13);
      this.lblNombre.TabIndex = 6;
      this.lblNombre.Text = "Nombre";
      // 
      // txtNombreBackup
      // 
      this.txtNombreBackup.Location = new System.Drawing.Point(12, 67);
      this.txtNombreBackup.Name = "txtNombreBackup";
      this.txtNombreBackup.Size = new System.Drawing.Size(132, 20);
      this.txtNombreBackup.TabIndex = 5;
      // 
      // Backups
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(509, 335);
      this.Controls.Add(this.lblNombre);
      this.Controls.Add(this.txtNombreBackup);
      this.Controls.Add(this.dgvBackups);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtDirectorio);
      this.Controls.Add(this.btnRestore);
      this.Controls.Add(this.btnBackup);
      this.Name = "Backups";
      this.Text = "Backup";
      ((System.ComponentModel.ISupportInitialize)(this.dgvBackups)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnBackup;
    private System.Windows.Forms.Button btnRestore;
    private System.Windows.Forms.TextBox txtDirectorio;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.DataGridView dgvBackups;
    private System.Windows.Forms.Label lblNombre;
    private System.Windows.Forms.TextBox txtNombreBackup;
  }
}