﻿namespace Consultorio.GUI
{
  partial class Permisos
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxFamilias = new System.Windows.Forms.GroupBox();
      this.cbxFamilias = new System.Windows.Forms.ComboBox();
      this.btnAsignarFamilia = new System.Windows.Forms.Button();
      this.gbxPatentes = new System.Windows.Forms.GroupBox();
      this.btnAsignarPatente = new System.Windows.Forms.Button();
      this.cbxPatentes = new System.Windows.Forms.ComboBox();
      this.lblUsuarios = new System.Windows.Forms.Label();
      this.treePermisos = new System.Windows.Forms.TreeView();
      this.cbxUsuarios = new System.Windows.Forms.ComboBox();
      this.gbxFamilias.SuspendLayout();
      this.gbxPatentes.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxFamilias
      // 
      this.gbxFamilias.Controls.Add(this.cbxFamilias);
      this.gbxFamilias.Controls.Add(this.btnAsignarFamilia);
      this.gbxFamilias.Location = new System.Drawing.Point(280, 158);
      this.gbxFamilias.Name = "gbxFamilias";
      this.gbxFamilias.Size = new System.Drawing.Size(188, 100);
      this.gbxFamilias.TabIndex = 14;
      this.gbxFamilias.TabStop = false;
      this.gbxFamilias.Text = "Familias";
      // 
      // cbxFamilias
      // 
      this.cbxFamilias.FormattingEnabled = true;
      this.cbxFamilias.Location = new System.Drawing.Point(6, 19);
      this.cbxFamilias.Name = "cbxFamilias";
      this.cbxFamilias.Size = new System.Drawing.Size(121, 21);
      this.cbxFamilias.TabIndex = 2;
      // 
      // btnAsignarFamilia
      // 
      this.btnAsignarFamilia.Location = new System.Drawing.Point(6, 46);
      this.btnAsignarFamilia.Name = "btnAsignarFamilia";
      this.btnAsignarFamilia.Size = new System.Drawing.Size(121, 23);
      this.btnAsignarFamilia.TabIndex = 2;
      this.btnAsignarFamilia.Text = "Asignar";
      this.btnAsignarFamilia.UseVisualStyleBackColor = true;
      this.btnAsignarFamilia.Click += new System.EventHandler(this.btnAsignarFamilia_Click);
      // 
      // gbxPatentes
      // 
      this.gbxPatentes.Controls.Add(this.btnAsignarPatente);
      this.gbxPatentes.Controls.Add(this.cbxPatentes);
      this.gbxPatentes.Location = new System.Drawing.Point(280, 52);
      this.gbxPatentes.Name = "gbxPatentes";
      this.gbxPatentes.Size = new System.Drawing.Size(188, 100);
      this.gbxPatentes.TabIndex = 13;
      this.gbxPatentes.TabStop = false;
      this.gbxPatentes.Text = "Patentes";
      // 
      // btnAsignarPatente
      // 
      this.btnAsignarPatente.Location = new System.Drawing.Point(6, 46);
      this.btnAsignarPatente.Name = "btnAsignarPatente";
      this.btnAsignarPatente.Size = new System.Drawing.Size(121, 23);
      this.btnAsignarPatente.TabIndex = 1;
      this.btnAsignarPatente.Text = "Asignar";
      this.btnAsignarPatente.UseVisualStyleBackColor = true;
      this.btnAsignarPatente.Click += new System.EventHandler(this.btnAsignarPatente_Click);
      // 
      // cbxPatentes
      // 
      this.cbxPatentes.FormattingEnabled = true;
      this.cbxPatentes.Location = new System.Drawing.Point(6, 19);
      this.cbxPatentes.Name = "cbxPatentes";
      this.cbxPatentes.Size = new System.Drawing.Size(121, 21);
      this.cbxPatentes.TabIndex = 0;
      // 
      // lblUsuarios
      // 
      this.lblUsuarios.AutoSize = true;
      this.lblUsuarios.Location = new System.Drawing.Point(12, 9);
      this.lblUsuarios.Name = "lblUsuarios";
      this.lblUsuarios.Size = new System.Drawing.Size(48, 13);
      this.lblUsuarios.TabIndex = 12;
      this.lblUsuarios.Text = "Usuarios";
      // 
      // treePermisos
      // 
      this.treePermisos.Location = new System.Drawing.Point(12, 52);
      this.treePermisos.Name = "treePermisos";
      this.treePermisos.Size = new System.Drawing.Size(262, 259);
      this.treePermisos.TabIndex = 11;
      // 
      // cbxUsuarios
      // 
      this.cbxUsuarios.FormattingEnabled = true;
      this.cbxUsuarios.Location = new System.Drawing.Point(12, 25);
      this.cbxUsuarios.Name = "cbxUsuarios";
      this.cbxUsuarios.Size = new System.Drawing.Size(121, 21);
      this.cbxUsuarios.TabIndex = 10;
      this.cbxUsuarios.SelectedValueChanged += new System.EventHandler(this.cbxUsuarios_SelectedValueChanged);
      // 
      // Permisos
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(483, 324);
      this.Controls.Add(this.gbxFamilias);
      this.Controls.Add(this.gbxPatentes);
      this.Controls.Add(this.lblUsuarios);
      this.Controls.Add(this.treePermisos);
      this.Controls.Add(this.cbxUsuarios);
      this.Name = "Permisos";
      this.Text = "Permisos";
      this.gbxFamilias.ResumeLayout(false);
      this.gbxPatentes.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxFamilias;
    private System.Windows.Forms.ComboBox cbxFamilias;
    private System.Windows.Forms.Button btnAsignarFamilia;
    private System.Windows.Forms.GroupBox gbxPatentes;
    private System.Windows.Forms.Button btnAsignarPatente;
    private System.Windows.Forms.ComboBox cbxPatentes;
    private System.Windows.Forms.Label lblUsuarios;
    private System.Windows.Forms.TreeView treePermisos;
    private System.Windows.Forms.ComboBox cbxUsuarios;
  }
}