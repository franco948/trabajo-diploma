﻿namespace Consultorio.GUI
{
  partial class Idiomas
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dgvTraducciones = new System.Windows.Forms.DataGridView();
      this.label1 = new System.Windows.Forms.Label();
      this.txtTag = new System.Windows.Forms.TextBox();
      this.txtTraduccion = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.btnAgregarTraduccion = new System.Windows.Forms.Button();
      this.btnAgregarIdioma = new System.Windows.Forms.Button();
      this.txtIdioma = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.btnAnterior = new System.Windows.Forms.Button();
      this.btnSiguiente = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dgvTraducciones)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvTraducciones
      // 
      this.dgvTraducciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvTraducciones.Location = new System.Drawing.Point(15, 187);
      this.dgvTraducciones.Name = "dgvTraducciones";
      this.dgvTraducciones.Size = new System.Drawing.Size(254, 142);
      this.dgvTraducciones.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 48);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(26, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Tag";
      // 
      // txtTag
      // 
      this.txtTag.Location = new System.Drawing.Point(15, 64);
      this.txtTag.Name = "txtTag";
      this.txtTag.Size = new System.Drawing.Size(124, 20);
      this.txtTag.TabIndex = 2;
      // 
      // txtTraduccion
      // 
      this.txtTraduccion.Location = new System.Drawing.Point(15, 103);
      this.txtTraduccion.Name = "txtTraduccion";
      this.txtTraduccion.Size = new System.Drawing.Size(124, 20);
      this.txtTraduccion.TabIndex = 4;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 87);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(61, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Traduccion";
      // 
      // btnAgregarTraduccion
      // 
      this.btnAgregarTraduccion.Location = new System.Drawing.Point(15, 129);
      this.btnAgregarTraduccion.Name = "btnAgregarTraduccion";
      this.btnAgregarTraduccion.Size = new System.Drawing.Size(124, 23);
      this.btnAgregarTraduccion.TabIndex = 5;
      this.btnAgregarTraduccion.Text = "Agregar Traduccion";
      this.btnAgregarTraduccion.UseVisualStyleBackColor = true;
      this.btnAgregarTraduccion.Click += new System.EventHandler(this.btnAgregarTraduccion_Click);
      // 
      // btnAgregarIdioma
      // 
      this.btnAgregarIdioma.Location = new System.Drawing.Point(15, 158);
      this.btnAgregarIdioma.Name = "btnAgregarIdioma";
      this.btnAgregarIdioma.Size = new System.Drawing.Size(124, 23);
      this.btnAgregarIdioma.TabIndex = 6;
      this.btnAgregarIdioma.Text = "Agregar Idioma";
      this.btnAgregarIdioma.UseVisualStyleBackColor = true;
      this.btnAgregarIdioma.Click += new System.EventHandler(this.btnAgregarIdioma_Click);
      // 
      // txtIdioma
      // 
      this.txtIdioma.Location = new System.Drawing.Point(15, 25);
      this.txtIdioma.Name = "txtIdioma";
      this.txtIdioma.Size = new System.Drawing.Size(124, 20);
      this.txtIdioma.TabIndex = 8;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 9);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(38, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Idioma";
      // 
      // btnAnterior
      // 
      this.btnAnterior.Location = new System.Drawing.Point(192, 129);
      this.btnAnterior.Name = "btnAnterior";
      this.btnAnterior.Size = new System.Drawing.Size(77, 23);
      this.btnAnterior.TabIndex = 9;
      this.btnAnterior.Text = "<<";
      this.btnAnterior.UseVisualStyleBackColor = true;
      this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
      // 
      // btnSiguiente
      // 
      this.btnSiguiente.Location = new System.Drawing.Point(192, 158);
      this.btnSiguiente.Name = "btnSiguiente";
      this.btnSiguiente.Size = new System.Drawing.Size(77, 23);
      this.btnSiguiente.TabIndex = 10;
      this.btnSiguiente.Text = ">>";
      this.btnSiguiente.UseVisualStyleBackColor = true;
      this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
      // 
      // Idiomas
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(283, 341);
      this.Controls.Add(this.btnSiguiente);
      this.Controls.Add(this.btnAnterior);
      this.Controls.Add(this.txtIdioma);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.btnAgregarIdioma);
      this.Controls.Add(this.btnAgregarTraduccion);
      this.Controls.Add(this.txtTraduccion);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.txtTag);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.dgvTraducciones);
      this.Name = "Idiomas";
      this.Text = "Idiomas";
      ((System.ComponentModel.ISupportInitialize)(this.dgvTraducciones)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvTraducciones;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtTag;
    private System.Windows.Forms.TextBox txtTraduccion;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btnAgregarTraduccion;
    private System.Windows.Forms.Button btnAgregarIdioma;
    private System.Windows.Forms.TextBox txtIdioma;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button btnAnterior;
    private System.Windows.Forms.Button btnSiguiente;
  }
}