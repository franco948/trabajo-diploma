﻿namespace Consultorio.GUI
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.btnLogin = new System.Windows.Forms.Button();
      this.txtContraseña = new System.Windows.Forms.TextBox();
      this.lblContraseña = new System.Windows.Forms.Label();
      this.txtNombreUsuario = new System.Windows.Forms.TextBox();
      this.lblNombreUsuario = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // btnLogin
      // 
      this.btnLogin.Location = new System.Drawing.Point(15, 129);
      this.btnLogin.Name = "btnLogin";
      this.btnLogin.Size = new System.Drawing.Size(100, 23);
      this.btnLogin.TabIndex = 9;
      this.btnLogin.Text = "Iniciar Sesion";
      this.btnLogin.UseVisualStyleBackColor = true;
      this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
      // 
      // txtContraseña
      // 
      this.txtContraseña.Location = new System.Drawing.Point(15, 64);
      this.txtContraseña.Name = "txtContraseña";
      this.txtContraseña.Size = new System.Drawing.Size(100, 20);
      this.txtContraseña.TabIndex = 8;
      // 
      // lblContraseña
      // 
      this.lblContraseña.AutoSize = true;
      this.lblContraseña.Location = new System.Drawing.Point(12, 48);
      this.lblContraseña.Name = "lblContraseña";
      this.lblContraseña.Size = new System.Drawing.Size(61, 13);
      this.lblContraseña.TabIndex = 7;
      this.lblContraseña.Text = "Contraseña";
      // 
      // txtNombreUsuario
      // 
      this.txtNombreUsuario.Location = new System.Drawing.Point(15, 25);
      this.txtNombreUsuario.Name = "txtNombreUsuario";
      this.txtNombreUsuario.Size = new System.Drawing.Size(100, 20);
      this.txtNombreUsuario.TabIndex = 6;
      // 
      // lblNombreUsuario
      // 
      this.lblNombreUsuario.AutoSize = true;
      this.lblNombreUsuario.Location = new System.Drawing.Point(12, 9);
      this.lblNombreUsuario.Name = "lblNombreUsuario";
      this.lblNombreUsuario.Size = new System.Drawing.Size(83, 13);
      this.lblNombreUsuario.TabIndex = 5;
      this.lblNombreUsuario.Text = "Nombre Usuario";
      // 
      // Login
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(417, 164);
      this.Controls.Add(this.btnLogin);
      this.Controls.Add(this.txtContraseña);
      this.Controls.Add(this.lblContraseña);
      this.Controls.Add(this.txtNombreUsuario);
      this.Controls.Add(this.lblNombreUsuario);
      this.Name = "Login";
      this.Text = "Login";
      this.Load += new System.EventHandler(this.Login_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.Label lblContraseña;
        private System.Windows.Forms.TextBox txtNombreUsuario;
        private System.Windows.Forms.Label lblNombreUsuario;
    }
}