﻿using Aplicacion.Entidades;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public static class Controlador
  {
    public static List<Traduccion> Traducciones { get; set; }

    public static void CargarGrilla(DataGridView unDataGridView,
      IEnumerable<object> unDataSource)
    {
      unDataGridView.DataSource = null;
      unDataGridView.DataSource = unDataSource;
    }

    public static void CambiarIdioma(Form unForm)
    {
      foreach (Control control in unForm.Controls)
        // remover if
        // cambiar control.Name por control.Tag
        if (Traducciones.Any(x => x.Tag.Nombre == control.Name))
          control.Text = Traducciones.
            FirstOrDefault(x => x.Tag.Nombre == control.Name).Texto;
    }

    //public static void Cargar(Main unMdiParent)
    //{
    //}

    //public static void Show<T>(IKernel unKernel, Form unMdiParent)
    //{
    //  string tipo = typeof(T).Name;

    //  Form form = (Form)Activator.
    //    CreateInstance(typeof(T), new object[] { unKernel });

    //  form.MdiParent = unMdiParent;
    //  form.Show();
    //}

    public static void AgregarVarios(Patente[] losPermisos, TreeView unTreeView)
    {
      foreach (Patente permiso in losPermisos)
      {
        var nodo = new TreeNode(permiso.Nombre);

        AgregarRecursivo(permiso, nodo);

        unTreeView.Nodes.Add(nodo);
      }
    }

    public static void AgregarRecursivo(Patente unPermiso, TreeNode unNodo)
    {
      if (unPermiso.DirectChilds != null)
      {
        foreach (var permiso in unPermiso.DirectChilds)
        {
          var childNode = new TreeNode(permiso.Nombre);

          unNodo.Nodes.Add(childNode);

          if (unPermiso.Listar().Count > 1)
            AgregarRecursivo(permiso, childNode);
        }
      }
    }
  }
}
