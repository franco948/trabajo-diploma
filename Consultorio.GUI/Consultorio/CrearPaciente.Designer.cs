﻿namespace Consultorio.GUI
{
  partial class CrearPaciente
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txtNombreApellido = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.cbxSexo = new System.Windows.Forms.ComboBox();
      this.btnCrearPaciente = new System.Windows.Forms.Button();
      this.lblDni = new System.Windows.Forms.Label();
      this.txtDni = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.txtEmail = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.txtTelefono = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
      this.SuspendLayout();
      // 
      // txtNombreApellido
      // 
      this.txtNombreApellido.Location = new System.Drawing.Point(15, 25);
      this.txtNombreApellido.Name = "txtNombreApellido";
      this.txtNombreApellido.Size = new System.Drawing.Size(121, 20);
      this.txtNombreApellido.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(92, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Nombre y Apellido";
      // 
      // cbxSexo
      // 
      this.cbxSexo.FormattingEnabled = true;
      this.cbxSexo.Location = new System.Drawing.Point(15, 232);
      this.cbxSexo.Name = "cbxSexo";
      this.cbxSexo.Size = new System.Drawing.Size(121, 21);
      this.cbxSexo.TabIndex = 2;
      // 
      // btnCrearPaciente
      // 
      this.btnCrearPaciente.Location = new System.Drawing.Point(15, 259);
      this.btnCrearPaciente.Name = "btnCrearPaciente";
      this.btnCrearPaciente.Size = new System.Drawing.Size(121, 50);
      this.btnCrearPaciente.TabIndex = 3;
      this.btnCrearPaciente.Tag = "btnCrear";
      this.btnCrearPaciente.Text = "Crear";
      this.btnCrearPaciente.UseVisualStyleBackColor = true;
      this.btnCrearPaciente.Click += new System.EventHandler(this.btnCrearPaciente_Click);
      // 
      // lblDni
      // 
      this.lblDni.AutoSize = true;
      this.lblDni.Location = new System.Drawing.Point(12, 51);
      this.lblDni.Name = "lblDni";
      this.lblDni.Size = new System.Drawing.Size(26, 13);
      this.lblDni.TabIndex = 5;
      this.lblDni.Text = "DNI";
      // 
      // txtDni
      // 
      this.txtDni.Location = new System.Drawing.Point(15, 67);
      this.txtDni.Name = "txtDni";
      this.txtDni.Size = new System.Drawing.Size(121, 20);
      this.txtDni.TabIndex = 4;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 93);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(32, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Email";
      // 
      // txtEmail
      // 
      this.txtEmail.Location = new System.Drawing.Point(15, 109);
      this.txtEmail.Name = "txtEmail";
      this.txtEmail.Size = new System.Drawing.Size(121, 20);
      this.txtEmail.TabIndex = 6;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 135);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(49, 13);
      this.label4.TabIndex = 9;
      this.label4.Text = "Telefono";
      // 
      // txtTelefono
      // 
      this.txtTelefono.Location = new System.Drawing.Point(15, 151);
      this.txtTelefono.Name = "txtTelefono";
      this.txtTelefono.Size = new System.Drawing.Size(121, 20);
      this.txtTelefono.TabIndex = 8;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(12, 177);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(93, 13);
      this.label5.TabIndex = 11;
      this.label5.Text = "Fecha Nacimiento";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(12, 216);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(31, 13);
      this.label6.TabIndex = 12;
      this.label6.Text = "Sexo";
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(298, 259);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(121, 50);
      this.button1.TabIndex = 13;
      this.button1.Tag = "btnCrear";
      this.button1.Text = "Cancelar";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // dtpFechaNacimiento
      // 
      this.dtpFechaNacimiento.Location = new System.Drawing.Point(15, 193);
      this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
      this.dtpFechaNacimiento.Size = new System.Drawing.Size(200, 20);
      this.dtpFechaNacimiento.TabIndex = 14;
      // 
      // CrearPaciente
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(432, 323);
      this.Controls.Add(this.dtpFechaNacimiento);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.txtTelefono);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtEmail);
      this.Controls.Add(this.lblDni);
      this.Controls.Add(this.txtDni);
      this.Controls.Add(this.btnCrearPaciente);
      this.Controls.Add(this.cbxSexo);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtNombreApellido);
      this.Name = "CrearPaciente";
      this.Text = "CrearPaciente";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtNombreApellido;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbxSexo;
    private System.Windows.Forms.Button btnCrearPaciente;
    private System.Windows.Forms.Label lblDni;
    private System.Windows.Forms.TextBox txtDni;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtEmail;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox txtTelefono;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
  }
}