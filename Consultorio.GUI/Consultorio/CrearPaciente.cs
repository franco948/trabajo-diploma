﻿using Consultorio.Entidades;
using Consultorio.Servicio.Interfaces;
using Ninject;
using System;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class CrearPaciente : Form
  {
    private IKernel _kernel;
    private IPacienteServicio _pacienteServicio;

    public CrearPaciente(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;
      _pacienteServicio = _kernel.Get<IPacienteServicio>();
      cbxSexo.Items.AddRange(Enum.GetNames(typeof(Generos)));
    }

    private void btnCrearPaciente_Click(object sender, EventArgs e)
    {
      Paciente paciente = new Paciente
      {
        NombreApellido = txtNombreApellido.Text,
        Dni = int.Parse(txtDni.Text),
        Email = txtEmail.Text,
        Telefono = int.Parse(txtTelefono.Text),
        FechaNacimiento = dtpFechaNacimiento.Value,
        Sexo = (Generos)Enum.Parse(typeof(Generos), (string)cbxSexo.SelectedItem)
      };

      _pacienteServicio.Agregar(paciente);
    }
  }
}
