﻿namespace Consultorio.GUI
{
  partial class CambiarIdioma
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblIdiomas = new System.Windows.Forms.Label();
      this.cbxIdiomas = new System.Windows.Forms.ComboBox();
      this.btnCambiar = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lblIdiomas
      // 
      this.lblIdiomas.AutoSize = true;
      this.lblIdiomas.Location = new System.Drawing.Point(12, 9);
      this.lblIdiomas.Name = "lblIdiomas";
      this.lblIdiomas.Size = new System.Drawing.Size(43, 13);
      this.lblIdiomas.TabIndex = 1;
      this.lblIdiomas.Text = "Idiomas";
      // 
      // cbxIdiomas
      // 
      this.cbxIdiomas.FormattingEnabled = true;
      this.cbxIdiomas.Location = new System.Drawing.Point(15, 25);
      this.cbxIdiomas.Name = "cbxIdiomas";
      this.cbxIdiomas.Size = new System.Drawing.Size(127, 21);
      this.cbxIdiomas.TabIndex = 2;
      // 
      // btnCambiar
      // 
      this.btnCambiar.Location = new System.Drawing.Point(15, 54);
      this.btnCambiar.Name = "btnCambiar";
      this.btnCambiar.Size = new System.Drawing.Size(127, 23);
      this.btnCambiar.TabIndex = 3;
      this.btnCambiar.Text = "Cambiar";
      this.btnCambiar.UseVisualStyleBackColor = true;
      this.btnCambiar.Click += new System.EventHandler(this.btnCambiar_Click);
      // 
      // CambiarIdioma
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(293, 89);
      this.Controls.Add(this.btnCambiar);
      this.Controls.Add(this.cbxIdiomas);
      this.Controls.Add(this.lblIdiomas);
      this.Name = "CambiarIdioma";
      this.Text = "CambiarIdioma";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblIdiomas;
    private System.Windows.Forms.ComboBox cbxIdiomas;
    private System.Windows.Forms.Button btnCambiar;
  }
}