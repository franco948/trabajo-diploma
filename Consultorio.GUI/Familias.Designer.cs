﻿namespace Consultorio.GUI
{
  partial class Familias
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxFamilias = new System.Windows.Forms.GroupBox();
      this.cbxFamilias = new System.Windows.Forms.ComboBox();
      this.btnAsignarFamilia = new System.Windows.Forms.Button();
      this.gbxPatentes = new System.Windows.Forms.GroupBox();
      this.btnAsignarPatente = new System.Windows.Forms.Button();
      this.cbxPatentes = new System.Windows.Forms.ComboBox();
      this.txtFamilia = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.btnRemoverPermiso = new System.Windows.Forms.Button();
      this.btnCrearFamilia = new System.Windows.Forms.Button();
      this.treePermisos = new System.Windows.Forms.TreeView();
      this.gbxFamilias.SuspendLayout();
      this.gbxPatentes.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxFamilias
      // 
      this.gbxFamilias.Controls.Add(this.cbxFamilias);
      this.gbxFamilias.Controls.Add(this.btnAsignarFamilia);
      this.gbxFamilias.Location = new System.Drawing.Point(188, 157);
      this.gbxFamilias.Name = "gbxFamilias";
      this.gbxFamilias.Size = new System.Drawing.Size(188, 100);
      this.gbxFamilias.TabIndex = 16;
      this.gbxFamilias.TabStop = false;
      this.gbxFamilias.Text = "Familias";
      // 
      // cbxFamilias
      // 
      this.cbxFamilias.FormattingEnabled = true;
      this.cbxFamilias.Location = new System.Drawing.Point(6, 19);
      this.cbxFamilias.Name = "cbxFamilias";
      this.cbxFamilias.Size = new System.Drawing.Size(121, 21);
      this.cbxFamilias.TabIndex = 2;
      // 
      // btnAsignarFamilia
      // 
      this.btnAsignarFamilia.Location = new System.Drawing.Point(6, 46);
      this.btnAsignarFamilia.Name = "btnAsignarFamilia";
      this.btnAsignarFamilia.Size = new System.Drawing.Size(121, 23);
      this.btnAsignarFamilia.TabIndex = 2;
      this.btnAsignarFamilia.Text = "Asignar";
      this.btnAsignarFamilia.UseVisualStyleBackColor = true;
      this.btnAsignarFamilia.Click += new System.EventHandler(this.btnAsignarFamilia_Click);
      // 
      // gbxPatentes
      // 
      this.gbxPatentes.Controls.Add(this.btnAsignarPatente);
      this.gbxPatentes.Controls.Add(this.cbxPatentes);
      this.gbxPatentes.Location = new System.Drawing.Point(188, 51);
      this.gbxPatentes.Name = "gbxPatentes";
      this.gbxPatentes.Size = new System.Drawing.Size(188, 100);
      this.gbxPatentes.TabIndex = 15;
      this.gbxPatentes.TabStop = false;
      this.gbxPatentes.Text = "Patentes";
      // 
      // btnAsignarPatente
      // 
      this.btnAsignarPatente.Location = new System.Drawing.Point(6, 46);
      this.btnAsignarPatente.Name = "btnAsignarPatente";
      this.btnAsignarPatente.Size = new System.Drawing.Size(121, 23);
      this.btnAsignarPatente.TabIndex = 1;
      this.btnAsignarPatente.Text = "Asignar";
      this.btnAsignarPatente.UseVisualStyleBackColor = true;
      this.btnAsignarPatente.Click += new System.EventHandler(this.btnAsignarPatente_Click);
      // 
      // cbxPatentes
      // 
      this.cbxPatentes.FormattingEnabled = true;
      this.cbxPatentes.Location = new System.Drawing.Point(6, 19);
      this.cbxPatentes.Name = "cbxPatentes";
      this.cbxPatentes.Size = new System.Drawing.Size(121, 21);
      this.cbxPatentes.TabIndex = 0;
      // 
      // txtFamilia
      // 
      this.txtFamilia.Location = new System.Drawing.Point(15, 25);
      this.txtFamilia.Name = "txtFamilia";
      this.txtFamilia.Size = new System.Drawing.Size(121, 20);
      this.txtFamilia.TabIndex = 17;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(39, 13);
      this.label1.TabIndex = 18;
      this.label1.Text = "Familia";
      // 
      // btnRemoverPermiso
      // 
      this.btnRemoverPermiso.Location = new System.Drawing.Point(15, 292);
      this.btnRemoverPermiso.Name = "btnRemoverPermiso";
      this.btnRemoverPermiso.Size = new System.Drawing.Size(121, 23);
      this.btnRemoverPermiso.TabIndex = 2;
      this.btnRemoverPermiso.Text = "Remover";
      this.btnRemoverPermiso.UseVisualStyleBackColor = true;
      this.btnRemoverPermiso.Click += new System.EventHandler(this.btnRemoverPermiso_Click);
      // 
      // btnCrearFamilia
      // 
      this.btnCrearFamilia.Location = new System.Drawing.Point(15, 263);
      this.btnCrearFamilia.Name = "btnCrearFamilia";
      this.btnCrearFamilia.Size = new System.Drawing.Size(121, 23);
      this.btnCrearFamilia.TabIndex = 20;
      this.btnCrearFamilia.Text = "Crear Familia";
      this.btnCrearFamilia.UseVisualStyleBackColor = true;
      this.btnCrearFamilia.Click += new System.EventHandler(this.btnCrearFamilia_Click);
      // 
      // treePermisos
      // 
      this.treePermisos.Location = new System.Drawing.Point(15, 51);
      this.treePermisos.Name = "treePermisos";
      this.treePermisos.Size = new System.Drawing.Size(167, 206);
      this.treePermisos.TabIndex = 21;
      // 
      // Familias
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(393, 327);
      this.Controls.Add(this.treePermisos);
      this.Controls.Add(this.btnCrearFamilia);
      this.Controls.Add(this.btnRemoverPermiso);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtFamilia);
      this.Controls.Add(this.gbxFamilias);
      this.Controls.Add(this.gbxPatentes);
      this.Name = "Familias";
      this.Text = "Familias";
      this.gbxFamilias.ResumeLayout(false);
      this.gbxPatentes.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxFamilias;
    private System.Windows.Forms.ComboBox cbxFamilias;
    private System.Windows.Forms.Button btnAsignarFamilia;
    private System.Windows.Forms.GroupBox gbxPatentes;
    private System.Windows.Forms.Button btnAsignarPatente;
    private System.Windows.Forms.ComboBox cbxPatentes;
    private System.Windows.Forms.TextBox txtFamilia;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnRemoverPermiso;
    private System.Windows.Forms.Button btnCrearFamilia;
    private System.Windows.Forms.TreeView treePermisos;
  }
}