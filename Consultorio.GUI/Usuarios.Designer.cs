﻿namespace Consultorio.GUI
{
  partial class Usuarios
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dgvUsuarios = new System.Windows.Forms.DataGridView();
      this.lblUsuarios = new System.Windows.Forms.Label();
      this.txtNombreUsuario = new System.Windows.Forms.TextBox();
      this.txtContraseña = new System.Windows.Forms.TextBox();
      this.lblUsuario = new System.Windows.Forms.Label();
      this.lblContraseña = new System.Windows.Forms.Label();
      this.lblIdioma = new System.Windows.Forms.Label();
      this.cbxIdiomas = new System.Windows.Forms.ComboBox();
      this.btnAgregar = new System.Windows.Forms.Button();
      this.btnActualizar = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvUsuarios
      // 
      this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvUsuarios.Location = new System.Drawing.Point(171, 25);
      this.dgvUsuarios.Name = "dgvUsuarios";
      this.dgvUsuarios.Size = new System.Drawing.Size(493, 157);
      this.dgvUsuarios.TabIndex = 0;
      // 
      // lblUsuarios
      // 
      this.lblUsuarios.AutoSize = true;
      this.lblUsuarios.Location = new System.Drawing.Point(168, 9);
      this.lblUsuarios.Name = "lblUsuarios";
      this.lblUsuarios.Size = new System.Drawing.Size(48, 13);
      this.lblUsuarios.TabIndex = 1;
      this.lblUsuarios.Text = "Usuarios";
      // 
      // txtNombreUsuario
      // 
      this.txtNombreUsuario.Location = new System.Drawing.Point(15, 25);
      this.txtNombreUsuario.Name = "txtNombreUsuario";
      this.txtNombreUsuario.Size = new System.Drawing.Size(121, 20);
      this.txtNombreUsuario.TabIndex = 2;
      // 
      // txtContraseña
      // 
      this.txtContraseña.Location = new System.Drawing.Point(15, 64);
      this.txtContraseña.Name = "txtContraseña";
      this.txtContraseña.Size = new System.Drawing.Size(121, 20);
      this.txtContraseña.TabIndex = 3;
      // 
      // lblUsuario
      // 
      this.lblUsuario.AutoSize = true;
      this.lblUsuario.Location = new System.Drawing.Point(12, 9);
      this.lblUsuario.Name = "lblUsuario";
      this.lblUsuario.Size = new System.Drawing.Size(83, 13);
      this.lblUsuario.TabIndex = 5;
      this.lblUsuario.Text = "Nombre Usuario";
      // 
      // lblContraseña
      // 
      this.lblContraseña.AutoSize = true;
      this.lblContraseña.Location = new System.Drawing.Point(12, 48);
      this.lblContraseña.Name = "lblContraseña";
      this.lblContraseña.Size = new System.Drawing.Size(61, 13);
      this.lblContraseña.TabIndex = 6;
      this.lblContraseña.Text = "Contraseña";
      // 
      // lblIdioma
      // 
      this.lblIdioma.AutoSize = true;
      this.lblIdioma.Location = new System.Drawing.Point(12, 87);
      this.lblIdioma.Name = "lblIdioma";
      this.lblIdioma.Size = new System.Drawing.Size(38, 13);
      this.lblIdioma.TabIndex = 7;
      this.lblIdioma.Text = "Idioma";
      // 
      // cbxIdiomas
      // 
      this.cbxIdiomas.FormattingEnabled = true;
      this.cbxIdiomas.Location = new System.Drawing.Point(15, 103);
      this.cbxIdiomas.Name = "cbxIdiomas";
      this.cbxIdiomas.Size = new System.Drawing.Size(121, 21);
      this.cbxIdiomas.TabIndex = 8;
      // 
      // btnAgregar
      // 
      this.btnAgregar.Location = new System.Drawing.Point(15, 130);
      this.btnAgregar.Name = "btnAgregar";
      this.btnAgregar.Size = new System.Drawing.Size(121, 23);
      this.btnAgregar.TabIndex = 9;
      this.btnAgregar.Text = "Agregar";
      this.btnAgregar.UseVisualStyleBackColor = true;
      this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
      // 
      // btnActualizar
      // 
      this.btnActualizar.Location = new System.Drawing.Point(15, 159);
      this.btnActualizar.Name = "btnActualizar";
      this.btnActualizar.Size = new System.Drawing.Size(121, 23);
      this.btnActualizar.TabIndex = 10;
      this.btnActualizar.Text = "Actualizar";
      this.btnActualizar.UseVisualStyleBackColor = true;
      this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
      // 
      // Usuarios
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(680, 199);
      this.Controls.Add(this.btnActualizar);
      this.Controls.Add(this.btnAgregar);
      this.Controls.Add(this.cbxIdiomas);
      this.Controls.Add(this.lblIdioma);
      this.Controls.Add(this.lblContraseña);
      this.Controls.Add(this.lblUsuario);
      this.Controls.Add(this.txtContraseña);
      this.Controls.Add(this.txtNombreUsuario);
      this.Controls.Add(this.lblUsuarios);
      this.Controls.Add(this.dgvUsuarios);
      this.Name = "Usuarios";
      this.Text = "Usuarios";
      ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvUsuarios;
    private System.Windows.Forms.Label lblUsuarios;
    private System.Windows.Forms.TextBox txtNombreUsuario;
    private System.Windows.Forms.TextBox txtContraseña;
    private System.Windows.Forms.Label lblUsuario;
    private System.Windows.Forms.Label lblContraseña;
    private System.Windows.Forms.Label lblIdioma;
    private System.Windows.Forms.ComboBox cbxIdiomas;
    private System.Windows.Forms.Button btnAgregar;
    private System.Windows.Forms.Button btnActualizar;
  }
}