﻿using Aplicacion.Entidades;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Familias : Form
  {
    private IKernel _kernel;
    private IPermisoServicio _permisoServicio;

    private List<Patente> _permisos;
    private Familia _familia;

    public Familias(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;

      _permisoServicio = _kernel.Get<IPermisoServicio>();

      _permisos = _permisoServicio.ObtenerTodos().ToList();
      _familia = new Familia();

      foreach (var permiso in _permisos)
      {
        if (!(permiso is Familia))
          cbxPatentes.Items.Add(permiso);
        else
          cbxFamilias.Items.Add(permiso);
      }
    }

    private void Asignar(Patente unPermiso)
    {
      try
      {
        if (!_familia.Existe(unPermiso.Permiso))
        {
          _familia.Agregar(unPermiso);

          TreeNode nodo = new TreeNode(unPermiso.Nombre);

          Controlador.AgregarRecursivo(unPermiso, nodo);

          treePermisos.Nodes.Add(nodo);
        }
        else
          throw new InvalidOperationException("Permiso ya asignado");
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void btnAsignarPatente_Click(object sender, EventArgs e)
    {
      try
      {
        Patente patente = (Patente)cbxPatentes.SelectedItem;

        this.Asignar(patente);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void btnAsignarFamilia_Click(object sender, EventArgs e)
    {
      try
      {
        Familia familia = (Familia)cbxFamilias.SelectedItem;

        this.Asignar(familia);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void btnCrearFamilia_Click(object sender, EventArgs e)
    {
      try
      {
        _familia.Nombre = txtFamilia.Text;

        _permisoServicio.Agregar(_familia);

        cbxFamilias.Items.Add(_familia);

        _familia = new Familia();

        treePermisos.Nodes.Clear();
        txtFamilia.Clear();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void btnRemoverPermiso_Click(object sender, EventArgs e)
    {
      string nombrePermiso = treePermisos.SelectedNode.Text;
      _familia.DirectChilds.RemoveAll(x => x.Nombre == nombrePermiso);

      treePermisos.SelectedNode.Remove();
    }
  }
}
