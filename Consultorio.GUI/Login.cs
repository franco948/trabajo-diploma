﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Login : Form
  {
    private IUsuarioServicio _usuarioServicio;
    private ITraduccionServicio _traduccionServicio;

    public Login(IKernel unKernel)
    {
      InitializeComponent();

      txtContraseña.UseSystemPasswordChar = true;
      _usuarioServicio = unKernel.Get<IUsuarioServicio>();
      _traduccionServicio = unKernel.Get<ITraduccionServicio>();
    }

    private void btnLogin_Click(object sender, EventArgs e)
    {
      var usuario = new Usuario();
      usuario.NombreUsuario = txtNombreUsuario.Text;
      usuario.Contraseña = txtContraseña.Text;

      try
      {
        _usuarioServicio.Login(usuario);

        Main.Traducciones = _traduccionServicio.
          ObtenerPorIdioma((Idioma)
          ((Usuario)SessionSingleton.Obtener.Usuario).Idioma).
          ToList();

        MessageBox.Show("Se ha iniciado sesion");

        Main.Validar((Main)this.MdiParent);

        this.Close();
      }
      catch (IndexOutOfRangeException exception)
      {
        MessageBox.Show(exception.Message);
      }
      catch (InvalidOperationException exception)
      {
        MessageBox.Show(exception.Message);
      }
      catch (Exception exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void Login_Load(object sender, EventArgs e)
    {

    }
  }
}
