﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Backups : Form
  {
    private IKernel _kernel;

    private IBackupServicio _backupServicio;

    private List<Backup> _backups;

    public Backups(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;
      _backupServicio = _kernel.Get<IBackupServicio>();

      _backups = _backupServicio.ObtenerTodos().ToList();

      dgvBackups.MultiSelect = false;
      dgvBackups.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

      Controlador.CargarGrilla(dgvBackups, _backups);
    }

    private void btnBackup_Click(object sender, EventArgs e)
    {
      Backup backup = new Backup
      {
        Nombre = txtNombreBackup.Text,
        Directorio = txtDirectorio.Text,
        FechaHora = DateTime.Now
      };

      _backupServicio.Backup(backup);

      _backups.Add(backup);

      Controlador.CargarGrilla(dgvBackups, _backups);
    }

    private void btnRestore_Click(object sender, EventArgs e)
    {
      Backup backup = (Backup)dgvBackups.SelectedRows[0].DataBoundItem;

      _backupServicio.Restore(backup);

      SessionSingleton.Obtener.Logout();

      Main.Validar((Main)this.MdiParent);
    }
  }
}
