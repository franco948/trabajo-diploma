﻿using Aplicacion.Entidades;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Integridad : Form
  {
    private IKernel _kernel;
    private IUsuarioServicio _usuarioServicio;

    public Integridad(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;

      _usuarioServicio = _kernel.Get<IUsuarioServicio>();

      cbxTablas.DataSource = Enum.GetValues(typeof(Tablas));
    }

    private void btnVerificar_Click(object sender, EventArgs e)
    {
      try
      {
        dgvFilasAfectadas.DataSource = null;

        dynamic servicio;

        switch ((Tablas)cbxTablas.SelectedItem)
        {
          case Tablas.Usuarios:
            servicio = _usuarioServicio;
            break;
          default:
            servicio = _usuarioServicio;
            break;
        }

        dgvFilasAfectadas.DataSource = servicio.VerificarIntegridadHorizontal();

        servicio.VerificarIntegridadVertical();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }
  }
}
