﻿namespace Consultorio.GUI
{
  partial class Main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.sesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuLogin = new System.Windows.Forms.ToolStripMenuItem();
      this.menuLogout = new System.Windows.Forms.ToolStripMenuItem();
      this.seguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuPermisos = new System.Windows.Forms.ToolStripMenuItem();
      this.menuUsuarios = new System.Windows.Forms.ToolStripMenuItem();
      this.menuIntegridad = new System.Windows.Forms.ToolStripMenuItem();
      this.menuBackups = new System.Windows.Forms.ToolStripMenuItem();
      this.menuFamilias = new System.Windows.Forms.ToolStripMenuItem();
      this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuCambiarIdioma = new System.Windows.Forms.ToolStripMenuItem();
      this.menuIdiomas = new System.Windows.Forms.ToolStripMenuItem();
      this.pacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuCrearPaciente = new System.Windows.Forms.ToolStripMenuItem();
      this.menuStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sesiónToolStripMenuItem,
            this.seguridadToolStripMenuItem,
            this.configuracionToolStripMenuItem,
            this.pacientesToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(722, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // sesiónToolStripMenuItem
      // 
      this.sesiónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLogin,
            this.menuLogout});
      this.sesiónToolStripMenuItem.Name = "sesiónToolStripMenuItem";
      this.sesiónToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
      this.sesiónToolStripMenuItem.Text = "Sesión";
      // 
      // menuLogin
      // 
      this.menuLogin.Name = "menuLogin";
      this.menuLogin.Size = new System.Drawing.Size(112, 22);
      this.menuLogin.Text = "Login";
      this.menuLogin.Click += new System.EventHandler(this.menuLogin_Click);
      // 
      // menuLogout
      // 
      this.menuLogout.Name = "menuLogout";
      this.menuLogout.Size = new System.Drawing.Size(112, 22);
      this.menuLogout.Text = "Logout";
      this.menuLogout.Click += new System.EventHandler(this.menuLogout_Click);
      // 
      // seguridadToolStripMenuItem
      // 
      this.seguridadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPermisos,
            this.menuUsuarios,
            this.menuIntegridad,
            this.menuBackups,
            this.menuFamilias});
      this.seguridadToolStripMenuItem.Name = "seguridadToolStripMenuItem";
      this.seguridadToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
      this.seguridadToolStripMenuItem.Text = "Seguridad";
      // 
      // menuPermisos
      // 
      this.menuPermisos.Name = "menuPermisos";
      this.menuPermisos.Size = new System.Drawing.Size(152, 22);
      this.menuPermisos.Text = "Permisos";
      this.menuPermisos.Click += new System.EventHandler(this.menuPermisos_Click);
      // 
      // menuUsuarios
      // 
      this.menuUsuarios.Name = "menuUsuarios";
      this.menuUsuarios.Size = new System.Drawing.Size(152, 22);
      this.menuUsuarios.Text = "Usuarios";
      this.menuUsuarios.Click += new System.EventHandler(this.menuUsuarios_Click);
      // 
      // menuIntegridad
      // 
      this.menuIntegridad.Name = "menuIntegridad";
      this.menuIntegridad.Size = new System.Drawing.Size(152, 22);
      this.menuIntegridad.Text = "Integridad";
      this.menuIntegridad.Click += new System.EventHandler(this.menuIntegridad_Click);
      // 
      // menuBackups
      // 
      this.menuBackups.Name = "menuBackups";
      this.menuBackups.Size = new System.Drawing.Size(152, 22);
      this.menuBackups.Text = "Backups";
      this.menuBackups.Click += new System.EventHandler(this.menuBackups_Click);
      // 
      // menuFamilias
      // 
      this.menuFamilias.Name = "menuFamilias";
      this.menuFamilias.Size = new System.Drawing.Size(152, 22);
      this.menuFamilias.Text = "Familias";
      this.menuFamilias.Click += new System.EventHandler(this.menuFamilias_Click);
      // 
      // configuracionToolStripMenuItem
      // 
      this.configuracionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCambiarIdioma,
            this.menuIdiomas});
      this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
      this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
      this.configuracionToolStripMenuItem.Text = "Configuracion";
      // 
      // menuCambiarIdioma
      // 
      this.menuCambiarIdioma.Name = "menuCambiarIdioma";
      this.menuCambiarIdioma.Size = new System.Drawing.Size(159, 22);
      this.menuCambiarIdioma.Text = "Cambiar Idioma";
      this.menuCambiarIdioma.Click += new System.EventHandler(this.menuCambiarIdioma_Click);
      // 
      // menuIdiomas
      // 
      this.menuIdiomas.Name = "menuIdiomas";
      this.menuIdiomas.Size = new System.Drawing.Size(159, 22);
      this.menuIdiomas.Text = "Idiomas";
      this.menuIdiomas.Click += new System.EventHandler(this.menuIdiomas_Click);
      // 
      // pacientesToolStripMenuItem
      // 
      this.pacientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCrearPaciente});
      this.pacientesToolStripMenuItem.Name = "pacientesToolStripMenuItem";
      this.pacientesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
      this.pacientesToolStripMenuItem.Text = "Pacientes";
      // 
      // menuCrearPaciente
      // 
      this.menuCrearPaciente.Name = "menuCrearPaciente";
      this.menuCrearPaciente.Size = new System.Drawing.Size(152, 22);
      this.menuCrearPaciente.Text = "Crear Paciente";
      this.menuCrearPaciente.Click += new System.EventHandler(this.menuCrearPaciente_Click);
      // 
      // Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(722, 261);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "Main";
      this.Text = "Main";
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem sesiónToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem menuLogin;
    private System.Windows.Forms.ToolStripMenuItem menuLogout;
    private System.Windows.Forms.ToolStripMenuItem seguridadToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem menuPermisos;
    private System.Windows.Forms.ToolStripMenuItem menuUsuarios;
    private System.Windows.Forms.ToolStripMenuItem menuIntegridad;
    private System.Windows.Forms.ToolStripMenuItem menuBackups;
    private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem menuCambiarIdioma;
    private System.Windows.Forms.ToolStripMenuItem menuIdiomas;
    private System.Windows.Forms.ToolStripMenuItem menuFamilias;
    private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem menuCrearPaciente;
  }
}