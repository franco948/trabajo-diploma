﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Usuarios : Form
  {
    private IKernel _kernel;

    private IUsuarioServicio _usuarioServicio;
    private IIdiomaServicio _idiomaServicio;

    private List<Idioma> _idiomas;
    private List<Usuario> _usuarios;

    public Usuarios(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;

      _usuarioServicio = _kernel.Get<IUsuarioServicio>();
      _idiomaServicio = _kernel.Get<IIdiomaServicio>();

      _usuarios = (List<Usuario>)_usuarioServicio.ObtenerTodos();
      _idiomas = (List<Idioma>)_idiomaServicio.ObtenerTodos();

      cbxIdiomas.DataSource = _idiomas;

      foreach (Usuario usuario in _usuarios)
        usuario.Idioma = _idiomas.FirstOrDefault(i => i.Id == usuario.Idioma.Id);

      Controlador.CargarGrilla(dgvUsuarios, _usuarios);
    }

    private void btnAgregar_Click(object sender, EventArgs e)
    {
      Usuario usuario = new Usuario
      {
        NombreUsuario = txtNombreUsuario.Text,
        Contraseña = CryptoManager.Cifrar(txtContraseña.Text),
        Idioma = (Idioma)cbxIdiomas.SelectedItem
      };

      _usuarioServicio.Agregar(usuario);

      _usuarios.Add(usuario);

      Controlador.CargarGrilla(dgvUsuarios, _usuarios);
    }

    private void btnActualizar_Click(object sender, EventArgs e)
    {
      Usuario usuario = (Usuario)dgvUsuarios.SelectedRows[0].DataBoundItem;

      BitacoraUsuario bitacoraUsuario = new BitacoraUsuario
      {
        Usuario = (Usuario)usuario.Clone(),
        FechaModificacion = DateTime.Now
      };

      usuario.NombreUsuario = txtNombreUsuario.Text;
      usuario.Contraseña = CryptoManager.Cifrar(txtContraseña.Text);
      usuario.Idioma = (Idioma)cbxIdiomas.SelectedItem;

      _usuarioServicio.ActualizarYTrazar(usuario, bitacoraUsuario);
    }
  }
}
