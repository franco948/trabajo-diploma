﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Permisos : Form
  {
    private IKernel _kernel;

    private IPermisoServicio _permisoServicio;
    private IUsuarioServicio _usuarioServicio;

    private List<Usuario> _usuarios;
    private List<Patente> _permisos;

    public Permisos(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;

      _permisoServicio = _kernel.Get<IPermisoServicio>();
      _usuarioServicio = _kernel.Get<IUsuarioServicio>();

      _usuarios = _usuarioServicio.ObtenerTodos().ToList();
      _permisos = _permisoServicio.ObtenerTodos().ToList();

      cbxUsuarios.Items.AddRange(_usuarios.ToArray());

      foreach (var permiso in _permisos)
      {
        if (!(permiso is Familia))
          cbxPatentes.Items.Add(permiso);
        else
          cbxFamilias.Items.Add(permiso);
      }
    }

    private void btnAsignarPatente_Click(object sender, EventArgs e)
    {
      try
      {
        if (cbxPatentes.SelectedItem != null)
        {
          var permiso = (Patente)cbxPatentes.SelectedItem;

          this.AsignarPermiso(permiso);
        }
        else
          throw new InvalidOperationException("No se ha seleccionado una patente");
      }
      catch (InvalidOperationException exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void btnAsignarFamilia_Click(object sender, EventArgs e)
    {
      try
      {
        if (cbxFamilias.SelectedItem != null)
        {
          var permiso = (Familia)cbxFamilias.SelectedItem;

          this.AsignarPermiso(permiso);
        }
        else
          throw new InvalidOperationException("No se ha seleccionado una familia");
      }
      catch (InvalidOperationException exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void AsignarPermiso(Patente unPermiso)
    {
      try
      {
        if (cbxUsuarios.SelectedItem != null)
        {
          Usuario usuario = (Usuario)cbxUsuarios.SelectedItem;

          _permisoServicio.Asignar(usuario, unPermiso);

          usuario.Permisos.Add(unPermiso);

          TreeNode nodo = new TreeNode(unPermiso.Nombre);

          Controlador.AgregarRecursivo(unPermiso, nodo);

          treePermisos.Nodes.Add(nodo);

          if (SessionSingleton.Obtener.Usuario.Id == usuario.Id)
          {
            SessionSingleton.Obtener.Usuario.Permisos.Add(unPermiso);
            Main.Validar((Main)this.MdiParent);
          }
        }
        else
          throw new InvalidOperationException("Seleccione un usuario");
      }
      catch (InvalidOperationException exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void cbxUsuarios_SelectedValueChanged(object sender, EventArgs e)
    {
      treePermisos.Nodes.Clear();

      var usuario = (Usuario)cbxUsuarios.SelectedItem;

      if (usuario.Permisos == null)
        usuario.Permisos = _usuarioServicio.GetPermisos(usuario).ToList();

      Controlador.AgregarVarios(usuario.Permisos.ToArray(), treePermisos);
    }
  }
}
