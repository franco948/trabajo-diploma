﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using Ninject;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class CambiarIdioma : Form
  {
    private IKernel _kernel;
    private IUsuarioServicio _usuarioServicio;
    private ITraduccionServicio _traduccionServicio;
    private IIdiomaServicio _idiomaServicio;

    private IEnumerable<Idioma> _idiomas;

    public CambiarIdioma(IKernel unKernel)
    {
      InitializeComponent();

      _kernel = unKernel;
      _usuarioServicio = _kernel.Get<IUsuarioServicio>();
      _traduccionServicio = _kernel.Get<ITraduccionServicio>();
      _idiomaServicio = _kernel.Get<IIdiomaServicio>();

      _idiomas = _idiomaServicio.ObtenerTodos();

      cbxIdiomas.Items.AddRange(_idiomas.ToArray());
    }

    private void btnCambiar_Click(object sender, System.EventArgs e)
    {
      Idioma idioma = (Idioma)cbxIdiomas.SelectedItem;

      SessionSingleton.Obtener.Usuario.Idioma = idioma;

      _usuarioServicio.Actualizar((Usuario)SessionSingleton.Obtener.Usuario);

      Controlador.Traducciones = _traduccionServicio.ObtenerPorIdioma(idioma).ToList();
      Controlador.CambiarIdioma(this);
    }
  }
}
