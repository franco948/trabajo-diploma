﻿using Aplicacion.DependencyResolution;
using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using Consultorio.DependencyResolution;
using Consultorio.Entidades;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Consultorio.GUI
{
  public partial class Main : Form
  {
    private IKernel _kernel;

    public Main()
    {
      InitializeComponent();

      INinjectModule[] _modulos = {
        new RepositorioModule(),
        new ServicioModule(),
        new ConsultorioRepositorioModule(),
        new ConsultorioServicioModule()
      };

      _kernel = new StandardKernel(_modulos);

      this.IsMdiContainer = true;

      //int idiomaId = Convert.ToInt32(
      //  ConfigurationManager.AppSettings["idiomaDefault"]);

      //Controlador.Traducciones = (List<Traduccion>)
      //  _kernel.Get<ITraduccionRepositorio>()
      //  .ObtenerPorIdioma(new Idioma { Id = idiomaId });
      //Controlador.CambiarIdioma(this);

      Validar(this);
    }

    public static List<Traduccion> Traducciones { get; set; }

    public static void Validar(Main unMain)
    {
      unMain.menuLogin.Enabled = !SessionSingleton.Obtener.EstaLogueado();
      unMain.menuLogout.Enabled = SessionSingleton.Obtener.EstaLogueado();

      ValidarMenu(unMain.menuBackups, Patentes.Backups);
      ValidarMenu(unMain.menuCambiarIdioma, Patentes.CambiarIdioma);
      ValidarMenu(unMain.menuFamilias, Patentes.Familias);
      ValidarMenu(unMain.menuIdiomas, Patentes.Idiomas);
      ValidarMenu(unMain.menuIntegridad, Patentes.Integridad);
      ValidarMenu(unMain.menuPermisos, Patentes.Permisos);
      ValidarMenu(unMain.menuUsuarios, Patentes.Usuarios);
      ValidarMenu(unMain.menuCrearPaciente, ConsultorioPatentes.AltaPaciente);
    }

    private static void ValidarMenu(ToolStripMenuItem unMenu, Patentes unPermiso)
    {
      unMenu.Enabled = SessionSingleton.Obtener.EstaLogueado() &&
        SessionSingleton.Obtener.Validar(unPermiso);
    }

    private void menuLogin_Click(object sender, EventArgs e)
    {
      Show<Login>(_kernel);
    }

    private void menuLogout_Click(object sender, EventArgs e)
    {
      SessionSingleton.Obtener.Logout();

      Validar(this);
    }

    private void menuPermisos_Click(object sender, EventArgs e)
    {
      Show<Permisos>(_kernel);
    }

    private void menuUsuarios_Click(object sender, EventArgs e)
    {
      Show<Usuarios>(_kernel);
    }

    private void menuIntegridad_Click(object sender, EventArgs e)
    {
      Show<Integridad>(_kernel);
    }

    private void menuBackup_Click(object sender, EventArgs e)
    {
      Show<Backups>(_kernel);
    }

    private void menuBackups_Click(object sender, EventArgs e)
    {
      Show<Backups>(_kernel);
    }

    private void menuCambiarIdioma_Click(object sender, EventArgs e)
    {
      Show<CambiarIdioma>(_kernel);
    }

    private void menuIdiomas_Click(object sender, EventArgs e)
    {
      Show<Idiomas>(_kernel);
    }

    private void menuFamilias_Click(object sender, EventArgs e)
    {
      Show<Familias>(_kernel);
    }

    private void menuCrearPaciente_Click(object sender, EventArgs e)
    {
      Show<CrearPaciente>(_kernel);
    }

    public void Show<T>(IKernel unKernel)
    {
      string tipo = typeof(T).Name;

      Form form = (Form)Activator.
        CreateInstance(typeof(T), new object[] { unKernel });

      form.MdiParent = this;
      form.Show();
    }
  }
}
