﻿using Aplicacion.Entidades;
using System;

namespace Aplicacion.Seguridad
{
  public class Session
  {
    private Usuario _usuario;

    internal Session()
    {
    }

    public Usuario Usuario { get { return _usuario; } }

    public void Login(Usuario unUsuario)
    {
      if (!this.EstaLogueado())
        _usuario = unUsuario;
      else
        throw new InvalidOperationException("Ya hay una sesion");
    }

    public bool Validar(Patentes unPermiso)
    {
      foreach (Patente permiso in _usuario.Permisos)
      {
        if (permiso.Existe(unPermiso))
          return true;
      }

      return false;
    }

    public void Logout()
    {
      if (this.EstaLogueado())
        _usuario = null;
    }

    public bool EstaLogueado()
    {
      return _usuario != null;
    }
  }
}
