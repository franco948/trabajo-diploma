﻿namespace Aplicacion.Seguridad
{
  public static class SessionSingleton
  {
    private static Session _session;

    public static Session Obtener
    {
      get
      {
        if (_session == null)
          _session = new Session();

        return _session;
      }
    }
  }
}
