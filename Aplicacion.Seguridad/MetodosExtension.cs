﻿namespace Aplicacion.Seguridad
{
  internal static class MetodosExtension
  {
    public static string Sumar(this string[] variasCadenas)
    {
      var cadenaResultante = "";

      foreach (var cadena in variasCadenas)
      {
        cadenaResultante += cadena;
      }

      return cadenaResultante;
    }
  }
}
