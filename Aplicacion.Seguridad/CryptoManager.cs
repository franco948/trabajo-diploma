﻿using System.Security.Cryptography;
using System.Text;

namespace Aplicacion.Seguridad
{
  public static class CryptoManager
  {
    public static string Cifrar(string unaCadena)
    {
      var algoritmoHash = MD5.Create();

      var datos = algoritmoHash.ComputeHash(
          Encoding.UTF8.GetBytes(unaCadena));

      return CrearString(datos);
    }

    public static string Cifrar(string[] variasCadenas)
    {
      var cadenaCombinada = variasCadenas.Sumar();

      return Cifrar(cadenaCombinada);
    }

    public static bool Comparar(string unaCadena, string unaCadenaCifrada)
    {
      var cadenaCifrada = Cifrar(unaCadena);

      return cadenaCifrada == unaCadenaCifrada;
    }

    private static string CrearString(byte[] valor)
    {
      var stringBuilder = new StringBuilder();

      for (var i = 0; i < valor.Length; i++)
      {
        stringBuilder.Append(valor[i].ToString("x2"));
      }

      return stringBuilder.ToString();
    }
  }
}
