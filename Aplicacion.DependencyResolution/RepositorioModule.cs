﻿using Aplicacion.Data;
using Aplicacion.Repositorio;
using Ninject.Modules;

namespace Aplicacion.DependencyResolution
{
  public class RepositorioModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IUsuarioRepositorio>().To<UsuarioRepositorio>();
      Bind<IPermisoRepositorio>().To<PermisoRepositorio>();
      Bind<ITagRepositorio>().To<TagRepositorio>();
      Bind<IIdiomaRepositorio>().To<IdiomaRepositorio>();
      Bind<ITraduccionRepositorio>().To<TraduccionRepositorio>();
      Bind<IBitacoraRepositorio>().To<BitacoraRepositorio>();
      Bind<IBackupRepositorio>().To<BackupRepositorio>();
      Bind<IBitacoraUsuarioRepositorio>().To<BitacoraUsuarioRepositorio>();
    }
  }
}
