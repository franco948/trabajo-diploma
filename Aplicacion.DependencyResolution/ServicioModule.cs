﻿using Aplicacion.Servicio;
using Aplicacion.Servicio.Interfaces;
using Ninject.Modules;

namespace Aplicacion.DependencyResolution
{
  public class ServicioModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IUsuarioServicio>().To<UsuarioServicio>();
      Bind<IPermisoServicio>().To<PermisoServicio>();
      Bind<ITraduccionServicio>().To<TraduccionServicio>();
      Bind<IBitacoraServicio>().To<BitacoraServicio>();
      Bind<IBackupServicio>().To<BackupServicio>();
      Bind<IIdiomaServicio>().To<IdiomaServicio>();
      Bind<ITagServicio>().To<TagServicio>();
    }
  }
}
