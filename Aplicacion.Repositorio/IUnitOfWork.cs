﻿using System;

namespace Aplicacion.Repositorio
{
  public interface IUnitOfWork : IDisposable
  {
    void SaveChanges();
  }
}
