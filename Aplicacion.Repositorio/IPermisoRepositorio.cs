﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface IPermisoRepositorio : IRepositorio<Patente>
  {
    void Asignar(Usuario unUsuario, Patente unPermiso);
  }
}
