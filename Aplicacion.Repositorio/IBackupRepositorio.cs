﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface IBackupRepositorio : IRepositorio<Backup>
  {
    void Backup(Backup unBackup);
    void Restore(Backup unBackup);
  }
}
