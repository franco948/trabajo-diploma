﻿namespace Aplicacion.Repositorio
{
  public interface IUnitOfWorkFactory
  {
    IUnitOfWork Create();
  }
}
