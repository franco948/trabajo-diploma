﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface IBitacoraRepositorio : IRepositorio<Bitacora>
  {
  }
}
