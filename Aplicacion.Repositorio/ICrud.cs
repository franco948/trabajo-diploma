﻿using System.Collections.Generic;

namespace Aplicacion.Repositorio
{
  public interface ICrud<T>
  {
    int Agregar(T unaEntidad);
    int Eliminar(T unaEntidad);
    int Actualizar(T unaEntidad);

    T Obtener(T unaEntidad);
    IEnumerable<T> ObtenerTodos();
    //IEnumerable<T> ObtenerTodosPor(string unFiltro);
    //IEnumerable<T> ObtenerTodosPor(Func<T, bool> unFiltro);
  }
}
