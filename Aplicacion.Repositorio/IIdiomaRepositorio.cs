﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface IIdiomaRepositorio : IRepositorio<Idioma>
  {
  }
}
