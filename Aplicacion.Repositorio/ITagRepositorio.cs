﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface ITagRepositorio : IRepositorio<Tag>
  {
  }
}
