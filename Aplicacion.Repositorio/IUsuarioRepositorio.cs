﻿using Aplicacion.Entidades;
using System.Collections.Generic;

namespace Aplicacion.Repositorio
{
  public interface IUsuarioRepositorio : IRepositorio<Usuario>
  {
    Usuario ObtenerPorNombre(Usuario unUsuario);
    IEnumerable<Patente> GetPermisos(Usuario unUsuario);
    Idioma GetIdioma(Usuario unUsuario);
  }
}
