﻿using System.Collections.Generic;

namespace Aplicacion.Repositorio
{
  public interface IRepositorioVerificable
  {
    IEnumerable<string> ObtenerDigitosHorizontales();

    void CalcularDigitoVertical();
    string ObtenerDigitoVertical();
  }
}
