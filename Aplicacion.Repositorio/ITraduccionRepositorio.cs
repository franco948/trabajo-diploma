﻿using Aplicacion.Entidades;
using System.Collections.Generic;

namespace Aplicacion.Repositorio
{
  public interface ITraduccionRepositorio : IRepositorio<Traduccion>
  {
    IEnumerable<Traduccion> ObtenerPorIdioma(Idioma idioma);
  }
}
