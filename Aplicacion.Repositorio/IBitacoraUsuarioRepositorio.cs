﻿using Aplicacion.Entidades;

namespace Aplicacion.Repositorio
{
  public interface IBitacoraUsuarioRepositorio : IRepositorio<BitacoraUsuario>
  {
  }
}
