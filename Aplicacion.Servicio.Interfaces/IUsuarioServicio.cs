﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;

namespace Aplicacion.Servicio.Interfaces
{
  public interface IUsuarioServicio :
    IUsuarioRepositorio,
    IServicioVerificable<Usuario>,
    IBitacoraTraceableServicio<Usuario, BitacoraUsuario>
  {
    void Login(Usuario unUsuario);
  }
}
