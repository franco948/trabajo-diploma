﻿namespace Aplicacion.Servicio.Interfaces
{
  public interface IBitacoraTraceableServicio<TEntidad, TBitacora>
  {
    void ActualizarYTrazar(TEntidad unaEntidad, TBitacora unaBitacoraEntidad);
  }
}
