﻿using Aplicacion.Entidades;
using System.Collections.Generic;

namespace Aplicacion.Servicio.Interfaces
{
  public interface IServicioVerificable<T> where T : Verificable
  {
    IEnumerable<T> VerificarIntegridadHorizontal();
    void VerificarIntegridadVertical();
  }
}
