﻿using Aplicacion.Repositorio;
using Consultorio.Entidades;

namespace Consultorio.Repositorio
{
  public interface IPacienteRepositorio : IRepositorio<Paciente>
  {
  }
}
