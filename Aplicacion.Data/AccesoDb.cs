﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Aplicacion.Data
{
  public class AccesoDb
  {
    private SqlConnection _conexion;

    public AccesoDb()
    {
      var connectionString = ConfigurationManager.AppSettings["connectionString"];
      _conexion = new SqlConnection(connectionString);
    }

    public int Update(string unSql, IDataParameter[] parametros = null)
    {
      this.Open();

      var command = new SqlCommand(unSql, _conexion);

      if (parametros != null)
        command.Parameters.AddRange(parametros);

      int filasAfectadas = command.ExecuteNonQuery();

      this.Close();

      return filasAfectadas;
    }

    public DataTable GetTable(string unSql, IDataParameter[] parametros = null)
    {
      this.Open();

      var adapter = new SqlDataAdapter(unSql, _conexion);

      if (parametros != null)
        adapter.SelectCommand.Parameters.AddRange(parametros);

      var table = new DataTable();

      adapter.Fill(table);

      this.Close();

      return table;
    }

    public DataSet GetDataSet(string unSql, IDataParameter[] parametros = null)
    {
      this.Open();

      var adapter = new SqlDataAdapter(unSql, _conexion);

      if (parametros != null)
        adapter.SelectCommand.Parameters.AddRange(parametros);

      var set = new DataSet();

      adapter.Fill(set);

      this.Close();

      return set;
    }

    public IDataParameter CreateParameter(string unNombre, object unValor)
    {
      return new SqlParameter(unNombre, unValor);
    }

    private void Open()
    {
      if (_conexion.State != ConnectionState.Open)
        _conexion.Open();
    }

    private void Close()
    {
      if (_conexion.State != ConnectionState.Closed)
        _conexion.Close();
    }

    public object GetScalar(string sql)
    {
      this.Open();

      SqlCommand command = new SqlCommand(sql, _conexion);

      var scalar = command.ExecuteScalar();

      this.Close();

      return scalar;
    }

    //public int GetMaxId(string tabla)
    //{
    //  string sql =
    //    "SELECT MAX(id) " +
    //    "FROM " + tabla;

    //  return (int)this.GetScalar(sql);
    //}
  }
}
