﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System.Configuration;
using System.Data;

namespace Aplicacion.Data
{
  public class BackupRepositorio : Repositorio<Backup>, IBackupRepositorio
  {
    private string _dataBase;
    private string _backupDb;

    public BackupRepositorio()
    {
      _dataBase = ConfigurationManager.AppSettings["dataBase"];
      _backupDb = ConfigurationManager.AppSettings["backupDb"];
    }

    public override int Agregar(Backup unaEntidad)
    {
      string sql =
        "INSERT INTO Backups " +
        "(id, nombre, directorio, fecha_hora) " +
        "VALUES (@id, @nombre, @directorio, @fecha_hora)";

      IDataParameter[] parametros =
      {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@nombre", unaEntidad.Nombre),
        _acceso.CreateParameter("@directorio", unaEntidad.Directorio),
        _acceso.CreateParameter("@fecha_hora", unaEntidad.FechaHora)
      };

      return _acceso.Update(sql, parametros);
    }

    public void Backup(Backup unBackup)
    {
      string sql = string.Format(
        "BACKUP DATABASE {2} " +
        "TO DISK = '{0}\\{1}' "
        , unBackup.Directorio, unBackup.Nombre, _dataBase);

      _acceso.Update(sql);
    }

    public void Restore(Backup unBackup)
    {
      string sql = string.Format(
        "SELECT * " +
        "INTO {0}.[dbo].[Backups] " +
        "FROM {1}.[dbo].[Backups]; " +
        "USE master; " +
        "ALTER DATABASE {1} " +
        "SET SINGLE_USER WITH ROLLBACK IMMEDIATE; " +
        "RESTORE DATABASE {1} " +
        "FROM DISK = '{2}\\{3}' " +
        "WITH REPLACE; " +
        "ALTER DATABASE {0} " +
        "SET MULTI_USER; " +
        "DELETE FROM {1}.[dbo].Backups; " +
        "INSERT INTO {1}.[dbo].Backups " +
        "(nombre, directorio, fecha_hora) " +
        "SELECT nombre, directorio, fecha_hora " +
        "FROM {0}.[dbo].Backups; " +
        "DROP TABLE {0}.[dbo].Backups;",
        _backupDb, _dataBase, unBackup.Directorio, unBackup.Nombre);

      _acceso.Update(sql);
    }
  }
}
