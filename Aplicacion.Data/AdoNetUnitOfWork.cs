﻿using Aplicacion.Repositorio;
using System;
using System.Transactions;

namespace Aplicacion.Data
{
  public class AdoNetUnitOfWork : IUnitOfWork
  {
    private TransactionScope _transaccion;

    public AdoNetUnitOfWork()
    {
      _transaccion = new TransactionScope();
    }

    public void SaveChanges()
    {
      if (_transaccion != null)
      {
        _transaccion.Complete();
        _transaccion.Dispose();
      }
      else
        throw new InvalidOperationException("La transaccion ya fue completada");
    }

    public void Dispose()
    {
      _transaccion.Dispose();
    }
  }
}
