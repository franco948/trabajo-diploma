﻿using Aplicacion.Repositorio;

namespace Aplicacion.Data
{
  public class UnitOfWorkFactory : IUnitOfWorkFactory
  {
    public IUnitOfWork Create()
    {
      return new AdoNetUnitOfWork();
    }
  }
}
