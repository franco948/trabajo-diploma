﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class BitacoraRepositorio : Repositorio<Bitacora>, IBitacoraRepositorio
  {
    public override int Agregar(Bitacora unaEntidad)
    {
      unaEntidad.Id = Guid.NewGuid();

      string commandText =
        "INSERT INTO Bitacoras " +
        "(id, evento_id, fecha_hora, usuario_id) " +
        "VALUES (@id, @evento_id, @fecha_hora, @usuario_id)";

      IDataParameter[] parametros =
      {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@evento_id", unaEntidad.Evento),
        _acceso.CreateParameter("@fecha_hora", unaEntidad.FechaHora),
        _acceso.CreateParameter("@usuario_id", unaEntidad.Usuario.Id)
      };

      return _acceso.Update(commandText, parametros);
    }
  }
}
