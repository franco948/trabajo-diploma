﻿
using Aplicacion.Data.Mappers;
using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aplicacion.Data
{
  public class PermisoRepositorio : Repositorio<Patente>, IPermisoRepositorio
  {
    public override int Agregar(Patente unaEntidad)
    {
      string sql =
        "INSERT INTO Patentes " +
        "(id, nombre) VALUES (@id, @nombre);";

      unaEntidad.Id = Guid.NewGuid();

      List<IDataParameter> parametros = new List<IDataParameter> {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@nombre", unaEntidad.Nombre)
      };

      int counter = 0;
      foreach (Patente permiso in unaEntidad.DirectChilds)
      {
        sql += string.Format(
        "INSERT INTO Familias " +
        "(id, hijo_id) " +
        "VALUES (@id{0}, @hijo_id{0});", counter);

        parametros.AddRange(new IDataParameter[] {
          _acceso.CreateParameter("@id" + counter.ToString(), unaEntidad.Id),
          _acceso.CreateParameter("@hijo_id" + counter.ToString(), permiso.Id) });

        counter++;
      }

      return _acceso.Update(sql, parametros.ToArray());
    }

    public void Asignar(Usuario unUsuario, Patente unPermiso)
    {
      var commandText =
        "INSERT INTO Permisos " +
        "(usuario_id, permiso_id) " +
        "VALUES (@usuario_id, @permiso_id)";

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@usuario_id", unUsuario.Id),
        _acceso.CreateParameter("permiso_id", unPermiso.Id)
      };

      _acceso.Update(commandText, parametros);
    }

    public new IEnumerable<Patente> ObtenerTodos()
    {
      var commandText =
        "SELECT DISTINCT " +
        "Patentes.id, Familias.id AS familia_id, Patentes.nombre, Patentes.permiso " +
        "FROM Patentes " +
        "LEFT JOIN Familias " +
        "ON Patentes.id = Familias.id;" +
        "SELECT * FROM Familias";

      var dataset = _acceso.GetDataSet(commandText);

      var permisos = new List<Patente>();

      foreach (DataRow row in dataset.Tables[0].Rows)
      {
        var permiso = (Patente)
          MapperFactory.Obtener.Create<Patente>(_assembly).Map(row);

        permisos.Add(permiso);
      }

      foreach (DataRow row in dataset.Tables[1].Rows)
      {
        var familia = permisos.FirstOrDefault(x => x.Id == (Guid)row["id"]);

        var hijo = permisos.FirstOrDefault(x => x.Id == (Guid)row["hijo_id"]);

        familia.Agregar(hijo);
      }

      return permisos;
    }
  }
}
