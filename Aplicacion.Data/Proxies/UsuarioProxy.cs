﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System.Collections.Generic;

namespace Aplicacion.Data
{
  internal class UsuarioProxy : Usuario
  {
    internal IUsuarioRepositorio Repositorio { private get; set; }
    private List<Patente> _permisos;

    public UsuarioProxy()
    {
      _permisos = null;
    }

    public override List<Patente> Permisos
    {
      get
      {
        if (_permisos == null)
          _permisos = (List<Patente>)Repositorio.GetPermisos(this);

        return _permisos;
      }
      set { _permisos = value; }
    }
  }
}
