﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System.Data;

namespace Aplicacion.Data
{
  public class IdiomaRepositorio : Repositorio<Idioma>, IIdiomaRepositorio
  {
    public override int Agregar(Idioma unaEntidad)
    {
      string sql = string.Format(
        "INSERT INTO {0} " +
        "(id, nombre) VALUES (@id, @nombre)",
        _table);

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@nombre", unaEntidad.Nombre)
      };

      return _acceso.Update(sql, parametros);
    }
  }
}
