﻿using Aplicacion.Entidades;
using Aplicacion.Seguridad;
using System.Collections.Generic;
using System.Data;

namespace Aplicacion.Data
{
  public abstract class RepositorioVerificable<T> :
    Repositorio<T> where T : Entidad
  {
    public void CalcularDigitoVertical()
    {
      var digitosHorizontales = (List<string>)
        this.ObtenerDigitosHorizontales();

      string digitoVertical = CryptoManager.Cifrar(digitosHorizontales.ToArray());

      string sql = string.Format(
        "UPDATE DigitosVerticales " +
        "SET digito_vertical = '{0}' " +
        "WHERE tabla = '{1}'", digitoVertical, _table);

      _acceso.Update(sql);
    }

    public IEnumerable<string> ObtenerDigitosHorizontales()
    {
      string sql = "SELECT dvh FROM " + _table;

      DataTable tabla = _acceso.GetTable(sql);

      var digitosHorizontales = new List<string>();

      foreach (DataRow row in tabla.Rows)
      {
        digitosHorizontales.Add(!row.IsNull("dvh") ? (string)row["dvh"] : " ");
      }

      return digitosHorizontales;
    }

    public string ObtenerDigitoVertical()
    {
      string sql = string.Format(
        "SELECT digito_vertical " +
        "FROM DigitosVerticales " +
        "WHERE tabla = '{0}'", _table);

      return _acceso.GetScalar(sql).ToString();
    }
  }
}
