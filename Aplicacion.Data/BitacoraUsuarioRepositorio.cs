﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class BitacoraUsuarioRepositorio : Repositorio<BitacoraUsuario>,
    IBitacoraUsuarioRepositorio
  {
    public override int Agregar(BitacoraUsuario unaEntidad)
    {
      string sql = string.Format(
        "INSERT INTO {0} " +
        "(id, fecha_modificacion, nombre_usuario, contraseña, idioma_id, usuario_id) " +
        "VALUES (@id, @fecha_modificacion, " +
        "@nombre_usuario, @contraseña, @idioma_id, @usuario_id);",
        _table);

      unaEntidad.Id = Guid.NewGuid();

      IDataParameter[] parametros =
      {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@fecha_modificacion", unaEntidad.FechaModificacion),
        _acceso.CreateParameter("@nombre_usuario", unaEntidad.Usuario.NombreUsuario),
        _acceso.CreateParameter("@contraseña", unaEntidad.Usuario.Contraseña),
        _acceso.CreateParameter("@idioma_id", unaEntidad.Usuario.Idioma.Id),
        _acceso.CreateParameter("@usuario_id", unaEntidad.Usuario.Id)
      };

      return _acceso.Update(sql, parametros);
    }
  }
}
