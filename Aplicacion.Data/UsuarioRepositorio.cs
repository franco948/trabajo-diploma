﻿using Aplicacion.Data.Mappers;
using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aplicacion.Data
{
  public class UsuarioRepositorio : RepositorioVerificable<Usuario>,
    IUsuarioRepositorio,
    IRepositorioVerificable
  {
    private IPermisoRepositorio _permisoRepo;
    private IIdiomaRepositorio _idiomaRepo;

    public UsuarioRepositorio(
      IPermisoRepositorio unPermisoRepo,
      IIdiomaRepositorio unIdiomaRepo)
    {
      _permisoRepo = unPermisoRepo;
      _idiomaRepo = unIdiomaRepo;
    }

    public override int Agregar(Usuario unaEntidad)
    {
      var commandText =
        "INSERT INTO Usuarios " +
        "(id, nombre_usuario, contraseña, dvh, idioma_id) " +
        "VALUES (@id, @nombre_usuario, @contraseña, @dvh, @idioma_id);";

      unaEntidad.Dvh = unaEntidad.CalcularDvh();
      unaEntidad.Id = Guid.NewGuid();

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@id", unaEntidad.Id),
        _acceso.CreateParameter("@nombre_usuario", unaEntidad.NombreUsuario),
        _acceso.CreateParameter("@contraseña", unaEntidad.Contraseña),
        _acceso.CreateParameter("@dvh", unaEntidad.Dvh),
        _acceso.CreateParameter("@idioma_id", unaEntidad.Idioma.Id)
      };

      int filasAfectadas = _acceso.Update(commandText, parametros);

      this.CalcularDigitoVertical();

      return filasAfectadas;
    }

    public Idioma GetIdioma(Usuario unUsuario)
    {
      return _idiomaRepo.Obtener((Idioma)unUsuario.Idioma);
    }

    public IEnumerable<Patente> GetPermisos(Usuario unUsuario)
    {
      string sql =
        "SELECT * FROM Permisos " +
        "WHERE usuario_id = @usuario_id";

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@usuario_id", unUsuario.Id)
      };

      DataTable table = _acceso.GetTable(sql, parametros);

      var permisos = _permisoRepo.ObtenerTodos();
      var permisosUsuario = new List<Patente>();

      foreach (DataRow row in table.Rows)
      {
        var permiso = permisos.FirstOrDefault(x => x.Id == (Guid)row["permiso_id"]);

        if (permiso != null)
          permisosUsuario.Add(permiso);
      }

      return permisosUsuario;
    }

    public Usuario ObtenerPorNombre(Usuario unUsuario)
    {
      var commandText = string.Format(
        "SELECT * FROM {0} " +
        "WHERE nombre_usuario = @nombre_usuario", _table);

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@nombre_usuario", unUsuario.NombreUsuario)
      };

      DataTable table = _acceso.GetTable(commandText, parametros.ToArray());

      if (table.Rows.Count > 0)
        return (Usuario)
          MapperFactory.Obtener.Create<Usuario>(_assembly).Map(table.Rows[0]);
      else
        return null;
    }

    public override int Actualizar(Usuario unaEntidad)
    {
      string sql = string.Format(
        "UPDATE {0} " +
        "SET nombre_usuario = @nombre_usuario, contraseña = @contraseña, " +
        "idioma_id = @idioma_id, dvh = @dvh " +
        "WHERE id = @id", _table);

      unaEntidad.Dvh = unaEntidad.CalcularDvh();

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@nombre_usuario", unaEntidad.NombreUsuario),
        _acceso.CreateParameter("@contraseña", unaEntidad.Contraseña),
        _acceso.CreateParameter("@idioma_id", unaEntidad.Idioma.Id),
        _acceso.CreateParameter("@dvh", unaEntidad.Dvh),
        _acceso.CreateParameter("id", unaEntidad.Id)
      };

      return _acceso.Update(sql, parametros);
    }
  }
}
