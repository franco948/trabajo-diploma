﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class TagMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      var tag = new Tag();
      tag.Id = (Guid)row["id"];
      tag.Nombre = row["nombre"].ToString();

      return tag;
    }
  }
}
