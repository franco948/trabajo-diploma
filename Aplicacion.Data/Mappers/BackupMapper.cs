﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class BackupMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      return new Backup
      {
        Id = (Guid)row["id"],
        Nombre = (string)row["nombre"],
        Directorio = (string)row["directorio"],
        FechaHora = (DateTime)row["fecha_hora"]
      };
    }
  }
}
