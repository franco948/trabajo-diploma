﻿using Aplicacion.Entidades;
using System.Data;

namespace Aplicacion.Data
{
  public class TraduccionMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      var traduccion = new Traduccion();
      traduccion.Texto = row["texto"].ToString();

      return traduccion;
    }
  }
}
