﻿using System.Reflection;

namespace Aplicacion.Data.Mappers
{
  internal class MapperFactory
  {
    private static MapperFactory _instancia;

    private MapperFactory()
    {
    }

    public static MapperFactory Obtener
    {
      get
      {
        if (_instancia == null)
          _instancia = new MapperFactory();

        return _instancia;
      }
    }

    public Mapper Create<T>(string assembly)
    {
      var nombre = string.Format(assembly + ".{0}Mapper", typeof(T).Name);

      return (Mapper)Assembly.GetExecutingAssembly().CreateInstance(nombre);
    }
  }
}
