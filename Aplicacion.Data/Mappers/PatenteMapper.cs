﻿using Aplicacion.Entidades;
using System;
using System.Data;
using System.Reflection;

namespace Aplicacion.Data
{
  public class PatenteMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      var permiso = row.IsNull("familia_id") ? new Patente() : new Familia();
      permiso.Id = (Guid)row["id"];
      permiso.Nombre = row["nombre"].ToString();

      object[] parametros = { (int)row["permiso"] };
      permiso.Permiso = (Patentes)
        Activator.CreateInstance
        (typeof(Patentes), BindingFlags.Instance | BindingFlags.NonPublic,
        null, parametros, null, null);

      return permiso;
    }
  }
}
