﻿using System.Data;

namespace Aplicacion.Data
{
  public abstract class Mapper
  {
    public abstract object Map(DataRow row);
  }
}
