﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class BitacoraUsuarioMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      UsuarioMapper usuarioMapper = new UsuarioMapper();

      return new BitacoraUsuario
      {
        FechaModificacion = (DateTime)row["fecha_modificacion"],
        Usuario = (Usuario)usuarioMapper.Map(row)
      };
    }
  }
}
