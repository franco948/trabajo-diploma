﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class IdiomaMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      return new Idioma
      {
        Id = (Guid)row["id"],
        Nombre = row["nombre"].ToString(),
      };
    }
  }
}
