﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data
{
  public class UsuarioMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      var usuario = new Usuario
      {
        Id = (Guid)row["id"],
        NombreUsuario = (string)row["nombre_usuario"],
        Contraseña = (string)row["contraseña"],
        Idioma = new Idioma() { Id = (Guid)row["idioma_id"] },
        Dvh = !row.IsNull("dvh") ? (string)row["dvh"] : ""
      };

      return usuario;
    }
  }
}
