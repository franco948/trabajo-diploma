﻿using Aplicacion.Entidades;
using System;
using System.Data;

namespace Aplicacion.Data.Mappers
{
  public class BitacoraMapper : Mapper
  {
    public override object Map(DataRow row)
    {
      Bitacora bitacora = new Bitacora
      {
        Evento = (Eventos)row["evento_id"],
        //Descripcion = (string)row["descripcion"],
        FechaHora = (DateTime)row["fechaHora"]
      };

      bitacora.Id = (Guid)row["usuario_id"];

      return bitacora;
    }
  }
}
