﻿using Aplicacion.Data.Mappers;
using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Aplicacion.Data
{
  public class TraduccionRepositorio : Repositorio<Traduccion>, ITraduccionRepositorio
  {
    private ITagRepositorio _tagRepo;

    public TraduccionRepositorio(ITagRepositorio unTagRepo)
    {
      _tagRepo = unTagRepo;
    }

    public override int Agregar(Traduccion unaEntidad)
    {
      string sql =
        "INSERT INTO Traducciones " +
        "(idioma_id, tag_id, texto) " +
        "VALUES (@idioma_id, @tag_id, @texto)";

      IDataParameter[] parametros = {
        _acceso.CreateParameter("@idioma_id", unaEntidad.Idioma.Id),
        _acceso.CreateParameter("@tag_id", unaEntidad.Tag.Id),
        _acceso.CreateParameter("@texto", unaEntidad.Texto)
      };

      return _acceso.Update(sql, parametros);
    }

    public IEnumerable<Traduccion> ObtenerPorIdioma(Idioma idioma)
    {
      string sql =
        "SELECT * " +
        "FROM Traducciones " +
        "WHERE idioma_id = @idioma_id";

      IDataParameter[] parametros =
      {
        _acceso.CreateParameter("@idioma_id", idioma.Id)
      };

      DataTable table = _acceso.GetTable(sql, parametros);

      var tags = _tagRepo.ObtenerTodos();

      var traducciones = new List<Traduccion>();

      foreach (DataRow row in table.Rows)
      {
        var traduccion = (Traduccion)MapperFactory.Obtener.Create<Traduccion>(_assembly).Map(row);
        traduccion.Tag = tags.FirstOrDefault(x => x.Id == (Guid)row["tag_id"]);
        traduccion.Idioma = idioma;

        traducciones.Add(traduccion);
      }

      return traducciones;
    }
  }
}
