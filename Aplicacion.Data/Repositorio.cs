﻿using Aplicacion.Data.Mappers;
using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Aplicacion.Data
{
  public abstract class Repositorio<T> : IRepositorio<T> where T : Entidad
  {
    protected AccesoDb _acceso;
    protected string _table;
    protected string _assembly;

    public Repositorio()
    {
      _acceso = new AccesoDb();
      _table = typeof(T).Name + "s";
      _assembly = Assembly.GetExecutingAssembly().GetName().Name;
    }

    public virtual int Actualizar(T unaEntidad)
    {
      throw new NotImplementedException();
    }

    public virtual int Agregar(T unaEntidad)
    {
      throw new NotImplementedException();
    }

    public virtual int Eliminar(T unaEntidad)
    {
      throw new NotImplementedException();
    }

    public T Obtener(T unaEntidad)
    {
      var commandText =
         "SELECT * FROM " + _table +
         " WHERE Id = " + unaEntidad.Id;

      var table = _acceso.GetTable(commandText);

      if (table.Rows.Count > 0)
        return (T)MapperFactory.Obtener.Create<T>(_assembly).Map(table.Rows[0]);
      else
        return null;
    }

    public IEnumerable<T> ObtenerTodos()
    {
      var commandText = "SELECT * FROM " + _table;

      var table = _acceso.GetTable(commandText);

      var entidades = new List<T>();

      foreach (DataRow row in table.Rows)
      {
        var entidad = (T)MapperFactory
          .Obtener.Create<T>(_assembly).Map(row);

        entidades.Add(entidad);
      }

      return entidades;
    }
  }
}
