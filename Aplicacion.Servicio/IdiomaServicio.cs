﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;

namespace Aplicacion.Servicio
{
  public class IdiomaServicio : Servicio<Idioma>, IIdiomaServicio
  {
    private IIdiomaRepositorio _idiomaRepo;

    public IdiomaServicio(IIdiomaRepositorio unRepo) : base(unRepo)
    {
      _idiomaRepo = unRepo;
    }
  }
}
