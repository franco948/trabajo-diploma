﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;
using System.Collections.Generic;

namespace Aplicacion.Servicio
{
  public class TraduccionServicio : Servicio<Traduccion>, ITraduccionServicio
  {
    private ITraduccionRepositorio _traduccionRepo;

    public TraduccionServicio(ITraduccionRepositorio unRepositorio) :
      base(unRepositorio)
    {
      _traduccionRepo = unRepositorio;
    }

    public IEnumerable<Traduccion> ObtenerPorIdioma(Idioma idioma)
    {
      return _traduccionRepo.ObtenerPorIdioma(idioma);
    }


  }
}
