﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;

namespace Aplicacion.Servicio
{
  public class BackupServicio : Servicio<Backup>, IBackupServicio
  {
    private IBackupRepositorio _backupRepo;

    public BackupServicio(IBackupRepositorio unBackupRepo) : base(unBackupRepo)
    {
      _backupRepo = unBackupRepo;
    }

    public void Backup(Backup unBackup)
    {
      _backupRepo.Backup(unBackup);

      _backupRepo.Agregar(unBackup);
    }

    public void Restore(Backup unBackup)
    {
      _backupRepo.Restore(unBackup);
    }
  }
}
