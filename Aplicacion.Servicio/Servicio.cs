﻿using Aplicacion.Repositorio;
using System.Collections.Generic;
using System.Reflection;

namespace Aplicacion.Servicio
{
  public abstract class Servicio<T> : ICrud<T>
  {
    protected IRepositorio<T> _repositorio;
    protected IUnitOfWorkFactory _unitOfWorkFactory;

    public Servicio(IRepositorio<T> unRepositorio)
    {
      _repositorio = unRepositorio;

      string nombre = "Aplicacion.Data.UnitOfWorkFactory";
      _unitOfWorkFactory = (IUnitOfWorkFactory)
        Assembly.Load("Aplicacion.Data").CreateInstance(nombre);
    }

    public int Actualizar(T unaEntidad)
    {
      return _repositorio.Actualizar(unaEntidad);
    }

    public int Agregar(T unaEntidad)
    {
      return _repositorio.Agregar(unaEntidad);
    }

    public int Eliminar(T unaEntidad)
    {
      return _repositorio.Eliminar(unaEntidad);
    }

    public T Obtener(T unaEntidad)
    {
      return _repositorio.Obtener(unaEntidad);
    }

    public IEnumerable<T> ObtenerTodos()
    {
      return _repositorio.ObtenerTodos();
    }
  }
}
