﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aplicacion.Servicio
{
  public class UsuarioServicio : ServicioVerificable<Usuario>, IUsuarioServicio
  {
    private IUsuarioRepositorio _usuarioRepo;
    private IBitacoraRepositorio _bitacoraRepo;
    private IBitacoraUsuarioRepositorio _bitacoraUsuarioRepo;

    public UsuarioServicio(IUsuarioRepositorio unUsuarioRepo,
      IBitacoraRepositorio unBitacoraRepo,
      IBitacoraUsuarioRepositorio unBitacoraUsuarioRepo) : base(unUsuarioRepo)
    {
      _usuarioRepo = unUsuarioRepo;
      _bitacoraRepo = unBitacoraRepo;
      _bitacoraUsuarioRepo = unBitacoraUsuarioRepo;
    }

    public IEnumerable<Patente> GetPermisos(Usuario unUsuario)
    {
      return _usuarioRepo.GetPermisos(unUsuario);
    }

    public void Login(Usuario unUsuario)
    {
      Usuario usuario = _usuarioRepo.ObtenerPorNombre(unUsuario);

      Bitacora bitacora = new Bitacora
      {
        Evento = Eventos.Login,
        Usuario = usuario
      };

      if (usuario != null)
      {
        bool coinciden = CryptoManager.
          Comparar(unUsuario.Contraseña, usuario.Contraseña);

        if (coinciden)
        {
          usuario.Permisos = _usuarioRepo.GetPermisos(usuario).ToList();

          SessionSingleton.Obtener.Login(usuario);
          _bitacoraRepo.Agregar(bitacora);
        }
        else
        {
          _bitacoraRepo.Agregar(bitacora);
          throw new InvalidOperationException("Contraseña incorrecta");
        }
      }
      else
        throw new IndexOutOfRangeException("Usuario no existe");
    }

    public Usuario ObtenerPorNombre(Usuario unUsuario)
    {
      return _usuarioRepo.ObtenerPorNombre(unUsuario);
    }

    public Idioma GetIdioma(Usuario unUsuario)
    {
      throw new NotImplementedException();
    }

    public void ActualizarYTrazar(Usuario unaEntidad, BitacoraUsuario unaBitacoraEntidad)
    {
      _usuarioRepo.Actualizar(unaEntidad);
      _bitacoraUsuarioRepo.Agregar(unaBitacoraEntidad);
    }
  }
}

