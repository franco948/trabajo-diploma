﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;

namespace Aplicacion.Servicio
{
  public class TagServicio : Servicio<Tag>, ITagServicio
  {
    public TagServicio(ITagRepositorio unRepositorio) : base(unRepositorio)
    {
    }
  }
}
