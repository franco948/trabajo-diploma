﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;
using System;

namespace Aplicacion.Servicio
{
  public class PermisoServicio : Servicio<Patente>, IPermisoServicio
  {
    private IPermisoRepositorio _permisoRepositorio;

    public PermisoServicio(IPermisoRepositorio unRepositorio) : base(unRepositorio)
    {
      _permisoRepositorio = unRepositorio;
    }

    public void Asignar(Usuario unUsuario, Patente unPermiso)
    {
      bool existe = false;
      int indice = 0;

      while (!existe && indice < unUsuario.Permisos.Count)
      {
        existe = unUsuario.Permisos[indice].Existe(unPermiso.Permiso);

        indice++;
      }

      if (!existe)
      {
        _permisoRepositorio.Asignar(unUsuario, unPermiso);
      }
      else
        throw new InvalidOperationException("El permiso ya se encuentra asignado");
    }
  }
}
