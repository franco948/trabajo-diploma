﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Seguridad;
using Aplicacion.Servicio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aplicacion.Servicio
{
  public abstract class ServicioVerificable<T> : Servicio<T>,
    IServicioVerificable<T> where T : Verificable
  {
    IRepositorioVerificable _repoVerificable;

    public ServicioVerificable(IRepositorio<T> unRepositorio) : base(unRepositorio)
    {
      _repoVerificable = (IRepositorioVerificable)unRepositorio;
    }

    public IEnumerable<T> VerificarIntegridadHorizontal()
    {
      var entidades = _repositorio.ObtenerTodos().ToArray();

      string[] digitosHorizontales =
        entidades.Select(x => x.CalcularDvh()).ToArray();

      var entidadesCorruptas = new List<T>();
      for (int indice = 0; indice < entidades.Count(); indice++)
      {
        if (entidades[indice].Dvh != digitosHorizontales[indice])
          entidadesCorruptas.Add(entidades[indice]);
      }

      return entidadesCorruptas;
    }

    public void VerificarIntegridadVertical()
    {
      var digitosHorizontales = _repoVerificable.ObtenerDigitosHorizontales();

      var digitoVertical =
        CryptoManager.Cifrar(digitosHorizontales.ToArray());

      var digitoVerticalEsperado = _repoVerificable.ObtenerDigitoVertical();

      if (digitoVertical != digitoVerticalEsperado)
        // agregar excepcion personalizada
        throw new Exception("La integridad fue violada");
    }
  }
}
