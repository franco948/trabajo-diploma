﻿using Aplicacion.Entidades;
using Aplicacion.Repositorio;
using Aplicacion.Servicio.Interfaces;

namespace Aplicacion.Servicio
{
  public class BitacoraServicio : Servicio<Bitacora>, IBitacoraServicio
  {
    public BitacoraServicio(IRepositorio<Bitacora> unRepositorio) : base(unRepositorio)
    {
    }
  }
}
